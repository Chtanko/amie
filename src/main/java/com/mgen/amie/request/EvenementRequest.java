package com.mgen.amie.request;

import com.mgen.amie.entity.LieuEntity;
import com.mgen.amie.entity.UtilisateurEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
@Getter
@Setter
public class EvenementRequest {
    @ApiModelProperty(hidden = true)
    private LocalDateTime dateCreationEvenement = LocalDateTime.now();
    private String label;
    private String description;
    private String date_debut;
    private String date_fin;
    private String heure_debut;
    private String heure_fin;
    private String lien_replay;
    private String lien_ressources;
    private byte[] image;
    private String statut;
    private List<Integer> idTypologieEvenements;
}
