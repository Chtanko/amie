package com.mgen.amie.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mgen.amie.composite.IdAjouterFavoris;
import com.mgen.amie.composite.IdCorrespondre;
import com.mgen.amie.composite.IdInscriptions;
import com.mgen.amie.dto.ajouterfavoris.AjouterFavorisDto;
import com.mgen.amie.dto.evenement.EvenementCompletDto;
import com.mgen.amie.dto.evenement.EvenementUpdateDto;
import com.mgen.amie.dto.inscriptions.InscriptionsDto;
import com.mgen.amie.dto.lieu.LieuDto;
import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.dto.typologieevenements.TypologieEvenementsDto;
import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import com.mgen.amie.entity.*;
import com.mgen.amie.repository.*;
import com.mgen.amie.request.EvenementRequest;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EvenementService {

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private LieuRepository lieuRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private TypologieEvenementsRepository typologieEvenementsRepository;

    @Autowired
    private CorrespondreRepository correspondreRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AttribuerRepository attribuerRepository;

    @Autowired
    AjouterFavorisRepository ajouterFavorisRepository;

    @Autowired
    private InscriptionsRepository inscriptionsRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CacheManager cacheManager;

    private static final Logger logger = LoggerFactory.getLogger(EvenementService.class);


    public List<InscriptionsEntity> getInscriptions() {
        return inscriptionsRepository.findAll();
    }

    public class UnauthorizedException extends RuntimeException {
        public UnauthorizedException(String message) {
            super(message);
        }
    }

    /*
     * Ce code prend en entrée l'identifiant d'un utilisateur,
     * utilise ce dernier pour récupérer un événement associé
     * à cet utilisateur et retourne un DTO complet de cet événement.
     */

    @Cacheable(value = "evenementsCache") // Cette annotation indique à Spring de mettre le résultat de cette méthode en cache
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<EvenementCompletDto> getEvenementsByIdUtilisateur(int idUtilisateur) {
        // Récupérer les événements associés à l'utilisateur
        List<EvenementEntity> evenementEntities = evenementRepository.findAllByUtilisateurEntity_IdUtilisateur(idUtilisateur);

        List<EvenementCompletDto> evenements = new ArrayList<>();

        for (EvenementEntity evenementEntity : evenementEntities) {
            EvenementCompletDto evenementDto = new EvenementCompletDto();
            BeanUtils.copyProperties(evenementEntity, evenementDto);

            if (evenementEntity.getUtilisateurEntity() != null) {
                UtilisateurDto utilisateurDto = new UtilisateurDto();
                BeanUtils.copyProperties(evenementEntity.getUtilisateurEntity(), utilisateurDto);

                List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(
                        evenementEntity.getUtilisateurEntity().getIdUtilisateur());

                List<RoleDto> rolesDto = attributions.stream()
                        .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                        .collect(Collectors.toList());
                utilisateurDto.setRoles(rolesDto);
                evenementDto.setUtilisateur(utilisateurDto);

            }

            LieuDto lieuDto = modelMapper.map(evenementEntity.getLieuEntity(), LieuDto.class);
            evenementDto.setLieu(lieuDto);

            evenements.add(evenementDto);
        }

        return evenements;
    }

    //définit une méthode publique qui renvoie une
    // liste d'objets AjouterFavorisDto et prend en entrée un identifiant d'utilisateur
    public List<AjouterFavorisDto> getEvenementsFavorisParUtilisateur(int idUtilisateur) throws NotFoundException {
        //méthode "findById" est fournie par l'interface JpaRepository et
        // renvoie un objet Optionnel qui peut contenir l'objet si l'identifiant
        // est valide ou un objet vide si l'identifiant ne correspond à aucun utilisateur
        UtilisateurEntity utilisateurEntity = utilisateurRepository.findById(idUtilisateur)
                .orElseThrow(() -> new NotFoundException("Utilisateur non trouvé"));

        // récupère tous les événements favoris pour l'utilisateur en appelant la méthode
        // "findAllByUtilisateurEntity" de l'interface JpaRepository
        List<AjouterFavorisEntity> favoris = ajouterFavorisRepository.findAllByUtilisateurEntity(utilisateurEntity);

        // mapper chaque objet AjouterFavorisEntity dans la liste "favoris" à un objet
        // AjouterFavorisDto correspondant. La méthode "map" est utilisée pour effectuer
        // cette opération en utilisant l'objet ModelMapper
        // qui permet de mapper les propriétés de chaque objet en fonction de leur nom
        List<AjouterFavorisDto> favorisDto = favoris.stream()
                .map(favori -> {
                    AjouterFavorisDto favoriDto = modelMapper.map(favori, AjouterFavorisDto.class);
                    EvenementCompletDto evenementDto = modelMapper.map(favori.getEvenement(), EvenementCompletDto.class);

                    //Cette partie du code vérifie si l'objet EvenementEntity associé à chaque objet
                    // AjouterFavorisEntity a un utilisateur associé. Si c'est le cas, l'objet utilisateur est
                    // mappé sur un objet UtilisateurDto, et la méthode "findAllByUtilisateurEntity_IdUtilisateur"
                    // est utilisée pour récupérer tous les objets AttribuerEntity associés à cet utilisateur
                    if (favori.getEvenement().getUtilisateurEntity() != null) {
                        UtilisateurDto utilisateurDto = modelMapper.map(
                                favori.getEvenement().getUtilisateurEntity(), UtilisateurDto.class);
                        List<AttribuerEntity> attributions =
                                attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(
                                favori.getEvenement().getUtilisateurEntity().getIdUtilisateur());
                        List<RoleDto> rolesDto = attributions.stream()
                                .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                                .collect(Collectors.toList());
                        //objets AttribuerEntity sont mappés sur des objets RoleDto et sont
                        // stockés dans une liste "rolesDto" qui est ensuite ajoutée à l'objet UtilisateurDto.
                        utilisateurDto.setRoles(rolesDto);

                        // Utiliser la relation @OneToMany pour obtenir les correspondreEntities

                        //l'objet UtilisateurDto est ajouté à l'objet EvenementCompletDto
                        evenementDto.setUtilisateur(utilisateurDto);
                    }

                    favoriDto.setEvenement(evenementDto);
                    favoriDto.setUtilisateur(modelMapper.map(favori.getUtilisateur(), UtilisateurDto.class));
                    return favoriDto;
                })
                .collect(Collectors.toList());

        return favorisDto;
    }

    @Cacheable(value = "evenementsCache") // Cette annotation indique à Spring de mettre le résultat de cette méthode en cache
    @Transactional
    public List<EvenementCompletDto> getAllEvenements() {
        List<EvenementEntity> evenementsEntities = evenementRepository.findAll();

        List<EvenementCompletDto> evenements = new ArrayList<>();

        for (EvenementEntity evenementEntity : evenementsEntities) {
            EvenementCompletDto evenementDto = new EvenementCompletDto();
            BeanUtils.copyProperties(evenementEntity, evenementDto);

            if (evenementEntity.getUtilisateurEntity() != null) {
                UtilisateurDto utilisateurDto = new UtilisateurDto();
                BeanUtils.copyProperties(evenementEntity.getUtilisateurEntity(), utilisateurDto);

                List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(
                        evenementEntity.getUtilisateurEntity().getIdUtilisateur());

                List<RoleDto> rolesDto = attributions.stream()
                        .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                        .collect(Collectors.toList());
                utilisateurDto.setRoles(rolesDto);
                evenementDto.setUtilisateur(utilisateurDto);
            }

            LieuDto lieuDto = modelMapper.map(evenementEntity.getLieuEntity(), LieuDto.class);
            BeanUtils.copyProperties(evenementEntity.getLieuEntity(), lieuDto);
            evenementDto.setLieu(lieuDto);

            // Utiliser la relation @OneToMany pour obtenir les correspondreEntities
            List<CorrespondreEntity> correspondreEntities = evenementEntity.getCorrespondreEntities();

            // Transformation des TypologieEvenementsEntity en TypologieEvenementsDto
            List<TypologieEvenementsDto> typologiesEvenementsDto = correspondreEntities.stream()
                    .map(correspondreEntity -> modelMapper.map(correspondreEntity.getTypologieEvenements(),
                            TypologieEvenementsDto.class))
                    .collect(Collectors.toList());

            // Ajout de la liste de TypologieEvenementsDto à evenementDto
            evenementDto.setTypologieEvenements(typologiesEvenementsDto);

            evenements.add(evenementDto);
        }
        return evenements;
    }

    @CacheEvict(value = "evenementsCache", allEntries = true) // Cette annotation indique à Spring d'effacer tout le cache "evenementsCache"
    @Transactional
    public String createEvenementById(int idUtilisateur, int idLieu, EvenementRequest evenementRequest) {

    try{
        System.out.println("Début de la création de l'événement");

        List<Integer> idTypologieEvenements = evenementRequest.getIdTypologieEvenements();

        // Création de l'événement
        EvenementEntity evenement = new EvenementEntity();
        evenement.setLabel(evenementRequest.getLabel());
        evenement.setDescription(evenementRequest.getDescription());
        evenement.setDate_debut(evenementRequest.getDate_debut());
        evenement.setDate_fin(evenementRequest.getDate_fin());
        evenement.setDateCreationEvenement(LocalDateTime.now());
        evenement.setHeure_debut(evenementRequest.getHeure_debut());
        evenement.setHeure_fin(evenementRequest.getHeure_fin());
        evenement.setLien_replay(evenementRequest.getLien_replay());
        evenement.setLien_ressources(evenementRequest.getLien_ressources());
        evenement.setStatut(evenementRequest.getStatut());
        evenement.setImage(evenementRequest.getImage());

        System.out.println("Événement créé : " + evenement);

        // Récupération et association du lieu à l'événement
        Optional<LieuEntity> optionalLieu = lieuRepository.findById(idLieu);
        if (!optionalLieu.isPresent()) {
            throw new RuntimeException("Le lieu associé à l'événement n'existe pas");
        }

        evenement.setLieuEntity(optionalLieu.get());

        // Récupération et association de l'utilisateur à l'événement
        Optional<UtilisateurEntity> optionalUtilisateur = utilisateurRepository.findById(idUtilisateur);
        if (!optionalUtilisateur.isPresent()) {
            throw new RuntimeException("L'utilisateur n'existe pas");
        }
        evenement.setUtilisateurEntity(optionalUtilisateur.get());
        evenement = evenementRepository.save(evenement);

        System.out.println("Lieu associé à l'événement : " + optionalLieu.get());

        // Récupération et association de la typologie d'événement à l'événement
        for (int idTypologieEvenement : idTypologieEvenements) {
            Optional<TypologieEvenementsEntity> optionalTypologieEvenement = typologieEvenementsRepository.findById(idTypologieEvenement);
            if (!optionalTypologieEvenement.isPresent()) {
                throw new RuntimeException("La typologie d'événement avec l'ID " + idTypologieEvenement + " n'existe pas");
            }

            // Création de la relation Correspondre
            CorrespondreEntity correspondre = new CorrespondreEntity(evenement, optionalTypologieEvenement.get());
            correspondre.setId(new IdCorrespondre(evenement.getIdEvenement(), optionalTypologieEvenement.get().getIdTypologieEvenements()));

            // Enregistrement de la relation Correspondre en base de données
            correspondreRepository.save(correspondre);
        }

        }catch(Exception e){
           e.printStackTrace(); // Affiche la trace de l'exception
            return "Problème lors de la création de l'événement";

        }

        return "L'évenement a bien été créé";
    }

    @Transactional
    public void inscriptionEvenement(int idUtilisateur, int idEvenement) throws NotFoundException{
        UtilisateurEntity utilisateur = utilisateurRepository.findByIdUtilisateur(idUtilisateur)
                .orElseThrow(() -> new NotFoundException("L'utilisateur n'existe pas"));

        EvenementEntity evenement = evenementRepository.findById(idEvenement)
                .orElseThrow(() -> new NotFoundException("L'événement n'existe pas"));

        IdInscriptions idInscriptions = new IdInscriptions(idUtilisateur, idEvenement);

        // Vérifie si l'utilisateur est déjà inscrit à l'événement
        if (inscriptionsRepository.existsById(idInscriptions)) {
            throw new IllegalStateException("L'utilisateur est déjà inscrit à cet événement");
        }

        InscriptionsEntity inscriptionsEntity = new InscriptionsEntity();
        inscriptionsEntity.setId(idInscriptions);
        inscriptionsEntity.setUtilisateur(utilisateur);
        inscriptionsEntity.setEvenement(evenement);
        inscriptionsEntity.setDateInscription(LocalDateTime.now());

        inscriptionsRepository.save(inscriptionsEntity);
    }


    // La transaction doit être démarrée avant la méthode, et être commitée après, grâce à l'annotation @Transactional
    @Transactional
    public List<InscriptionsDto> getAllInscriptions() throws NotFoundException {
        try {
            // Appel de la méthode findAll() du Repository pour récupérer toutes les entités Inscriptions
            List<InscriptionsEntity> inscriptionsEntityList = inscriptionsRepository.findAll();

            // Initialisation d'une liste vide pour stocker les DTO d'inscription
            List<InscriptionsDto> inscriptionsDtoList = new ArrayList<>();

            // Boucle sur toutes les entités d'inscription récupérées
            for (InscriptionsEntity inscriptionsEntity : inscriptionsEntityList) {
                InscriptionsDto inscriptionsDto = new InscriptionsDto();
                BeanUtils.copyProperties(inscriptionsEntity.getEvenementEntity(), inscriptionsDto);

                // définir les champs qui ne sont pas ambigus
                inscriptionsDto.setIdInscription(inscriptionsEntity.getId());

                // Vérification si l'entité d'inscription a une entité événement liée
                if(inscriptionsEntity.getEvenement() != null) {
                    EvenementCompletDto evenementDto = modelMapper.map(inscriptionsEntity.getEvenement(),
                            EvenementCompletDto.class);
                    BeanUtils.copyProperties(inscriptionsEntity.getEvenement(), evenementDto);

                    List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(
                            inscriptionsEntity.getUtilisateurEntity().getIdUtilisateur());

                    List<RoleDto> rolesDto = attributions.stream()
                            .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                            .collect(Collectors.toList());
                    evenementDto.setRoles(rolesDto);
                    inscriptionsDto.setUtilisateur(evenementDto.getUtilisateur());
                }

                // Vérification si l'entité d'inscription a une entité Utilisateur liée
                if(inscriptionsEntity.getUtilisateurEntity() != null) {
                    UtilisateurDto utilisateurDto = modelMapper.map(inscriptionsEntity.getUtilisateurEntity(),
                            UtilisateurDto.class);
                    BeanUtils.copyProperties(inscriptionsEntity.getUtilisateurEntity(), utilisateurDto);

                     inscriptionsDto.setIdUtilisateur(inscriptionsEntity.getUtilisateurEntity().getIdUtilisateur());
                     inscriptionsDto.setIdEvenement(inscriptionsEntity.getEvenement().getIdEvenement());
                }

                List<CorrespondreEntity> correspondreEntities = inscriptionsEntity.getCorrespondreEntities();

                List<TypologieEvenementsDto> typologieEvenementsDtoList = correspondreEntities.stream()
                        .map(correspondreEntity -> modelMapper.map(correspondreEntity.getTypologieEvenements(),
                                TypologieEvenementsDto.class))
                        .collect(Collectors.toList());
                inscriptionsDto.setTypologieEvenements(typologieEvenementsDtoList);

                LieuDto lieuDto = modelMapper.map(inscriptionsEntity.getLieuEntity(), LieuDto.class);
                BeanUtils.copyProperties(inscriptionsEntity.getLieuEntity(), lieuDto);
                inscriptionsDto.setLieu(lieuDto);

                inscriptionsDtoList.add(inscriptionsDto);
            }

            // Retourne la liste de DTO Inscriptions
            return inscriptionsDtoList;

        } catch (Exception e) {  // Si une exception est levée pendant le traitement
            e.printStackTrace(); // Affiche la trace de l'exception
            // Lance une nouvelle exception indiquant que les inscriptions n'ont pas été trouvées
            throw new NotFoundException("Aucune inscription trouvée");
        }
    }

    @Transactional
    public List<InscriptionsDto> getAllInscriptionsByIdUtilisateur(int idUtilisateur) throws NotFoundException {
        try {
            List<InscriptionsEntity> inscriptions = inscriptionsRepository.findAllByUtilisateurEntity_IdUtilisateur(idUtilisateur);

            List<InscriptionsDto> inscriptionsDtoList = new ArrayList<>();

            for (InscriptionsEntity inscription : inscriptions) {
                InscriptionsDto inscriptionsDto = new InscriptionsDto();
                BeanUtils.copyProperties(inscription.getEvenementEntity(), inscriptionsDto);

                if (inscription.getEvenementEntity() != null) {
                    UtilisateurDto utilisateurDto = modelMapper.map(inscription.getEvenementEntity().getUtilisateurEntity(), UtilisateurDto.class);
                    BeanUtils.copyProperties(inscription.getEvenementEntity().getUtilisateurEntity(), utilisateurDto);

                    List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(inscription.getEvenementEntity().getUtilisateurEntity().getIdUtilisateur());

                    List<RoleDto> rolesDto = attributions.stream()
                            .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                            .collect(Collectors.toList());

                    utilisateurDto.setRoles(rolesDto);
                    inscriptionsDto.setUtilisateur(utilisateurDto);
                }

                LieuDto lieuDto = modelMapper.map(inscription.getLieuEntity(), LieuDto.class);
                BeanUtils.copyProperties(inscription.getLieuEntity(), lieuDto);
                inscriptionsDto.setLieu(lieuDto);

                // Utiliser la relation @OneToMany pour obtenir les correspondreEntities
                List<CorrespondreEntity> correspondreEntities = inscription.getCorrespondreEntities();

                List<TypologieEvenementsDto> typologieEvenementsDtoList = correspondreEntities.stream()
                        .map(correspondreEntity -> modelMapper.map(correspondreEntity.getTypologieEvenements(), TypologieEvenementsDto.class))
                        .collect(Collectors.toList());

                inscriptionsDto.setIdEvenement(inscription.getEvenementEntity().getIdEvenement());
                inscriptionsDto.setIdUtilisateur(inscription.getUtilisateurEntity().getIdUtilisateur());

                inscriptionsDto.setTypologieEvenements(typologieEvenementsDtoList);

                // Ajouter la date de validation à la DTO
                inscriptionsDto.setDateValidation(inscription.getEvenementEntity().getDateValidation());

                inscriptionsDtoList.add(inscriptionsDto);
            }

            return inscriptionsDtoList;

        } catch (Exception e) {
            e.printStackTrace();
            throw new NotFoundException("Aucune inscription trouvée");
        }
    }

    @Transactional
    public String deleteInscription(int idInscriptions, int idUtilisateur) throws NotFoundException, UnauthorizedException {
        try {
            // Créer un nouvel IdInscriptions avec l'idUtilisateur et l'idInscriptions
            IdInscriptions idInscription = new IdInscriptions(idUtilisateur, idInscriptions);

            // Récupérer l'inscription
            InscriptionsEntity inscription = (InscriptionsEntity) inscriptionsRepository.findById(idInscription)
                    .orElseThrow(() -> new NotFoundException("L'inscription n'existe pas"));

            // Récupérer l'événement lié à l'inscription
            EvenementEntity evenement = inscription.getEvenement();

            // Vérifier si l'utilisateur est le créateur de l'inscription
            if (inscription.getUtilisateurEntity().getIdUtilisateur() == idUtilisateur) {
                // Si c'est le cas, supprimer l'inscription
                inscriptionsRepository.deleteById(idInscription);
            } else {
                // Sinon, lever une exception
                throw new UnauthorizedException("Seul le créateur de l'inscription peut supprimer cette inscription");
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public void ajouterEvenementFavoris(int idUtilisateur, int idEvenement) throws NotFoundException {
        //recuperer l'utilisateur
        UtilisateurEntity utilisateur = utilisateurRepository.findByIdUtilisateur(idUtilisateur)
                .orElseThrow(() -> new NotFoundException("L'utilisateur n'existe pas"));
        //recuperer l'evenement
        EvenementEntity evenement = evenementRepository.findById(idEvenement)
                .orElseThrow(() -> new NotFoundException("L'événement n'existe pas"));

        IdAjouterFavoris idAjouterFavoris = new IdAjouterFavoris(idUtilisateur, idEvenement);

        //verifier si l'ajout d'un favoris existe deja
        if(ajouterFavorisRepository.existsById(idAjouterFavoris)){
            throw new IllegalStateException("L'événement est déjà dans vos favoris");
        }

        //Creer une nouvelle instance de entité AjouterFavorisEntity
        AjouterFavorisEntity ajouterFavorisEntity = new AjouterFavorisEntity();
        //associer l'idAjouterFavoris à l'entité AjouterFavorisEntity
        ajouterFavorisEntity.setId(idAjouterFavoris);
        //associer l'utilisateur à l'ajout d'un favoris
        ajouterFavorisEntity.setUtilisateur(utilisateur);
        //associer le l'evenement à un ajout de favoris
        ajouterFavorisEntity.setEvenement(evenement);
        //definir la date d'ajout d'un favoris
        ajouterFavorisEntity.setDateAjout(LocalDateTime.now());

        //sauvegarde de l'entité AjouterFavorisEntity dans la base de données
        ajouterFavorisRepository.save(ajouterFavorisEntity);
    }

    @Transactional  // Démarre une transaction
    public String updateEvenement(int idUtilisateur, int idEvenement, EvenementUpdateDto evenementUpdateDto) throws NotFoundException {
        try {  // Début du bloc try pour gérer les exceptions
            // Récupère l'utilisateur par son ID, sinon lance une exception
            UtilisateurEntity utilisateur = utilisateurRepository.findByIdUtilisateur(idUtilisateur)
                    .orElseThrow(() -> new NotFoundException("L'utilisateur n'existe pas"));

            // Récupère l'événement par son ID, sinon lance une exception
            EvenementEntity evenement = evenementRepository.findById(idEvenement)
                    .orElseThrow(() -> new NotFoundException("L'événement n'existe pas"));

            // Vérifie si l'utilisateur est autorisé à modifier l'événement
            if (utilisateur.getIdUtilisateur() != evenement.getUtilisateurEntity().getIdUtilisateur()) {
                throw new RuntimeException("Vous n'êtes pas autorisé à modifier cet événement");
            }

            // Récupère l'ID du lieu à partir du DTO
            int idLieu = evenementUpdateDto.getIdLieu();
            // Récupère le lieu par son ID, sinon lance une exception
            LieuEntity lieu = lieuRepository.findByIdLieu(idLieu).orElseThrow(() ->
                    new NotFoundException("Le lieu n'existe pas"));

            // Met à jour les attributs de l'événement avec les valeurs du DTO
            evenement.setLieu(lieu);
            evenement.setLabel(evenementUpdateDto.getLabel());
            evenement.setDescription(evenementUpdateDto.getDescription());
            evenement.setDate_debut(evenementUpdateDto.getDate_debut());
            evenement.setDate_fin(evenementUpdateDto.getDate_fin());
            evenement.setHeure_debut(evenementUpdateDto.getHeure_debut());
            evenement.setHeure_fin(evenementUpdateDto.getHeure_fin());
            evenement.setLien_replay(evenementUpdateDto.getLien_replay());
            evenement.setLien_ressources(evenementUpdateDto.getLien_ressources());
            evenement.setImage(evenementUpdateDto.getImage());
            evenement.setStatut(evenementUpdateDto.getStatut());

            // Récupère les correspondances existantes pour l'événement
            List<CorrespondreEntity> existingCorrespondences = correspondreRepository.findAllByEvenementEntity(evenement);

            // Crée un ensemble avec les ID des nouvelles correspondances à partir du DTO
            Set<Integer> newCorrespondencesIds = new HashSet<>(evenementUpdateDto.getIdTypologieEvenements());

            // Parcourt les correspondances existantes
            for (CorrespondreEntity existingCorrespondence : existingCorrespondences) {
                // Récupère l'ID de la correspondance existante
                int existingCorrespondenceId = existingCorrespondence.getTypologieEvenements().getIdTypologieEvenements();
                // Si l'ID de la correspondance existante n'est pas dans le nouvel ensemble, supprime la correspondance
                if (!newCorrespondencesIds.contains(existingCorrespondenceId)) {
                    correspondreRepository.delete(existingCorrespondence);
                } else {
                    // Si l'ID de la correspondance existante est dans le nouvel ensemble, le retire de l'ensemble
                    newCorrespondencesIds.remove(existingCorrespondenceId);
                }
            }

            // Parcourt les ID restants dans le nouvel ensemble
            for (int newCorrespondenceId : newCorrespondencesIds) {
                // Récupère la typologie par son ID
                TypologieEvenementsEntity typologie = typologieEvenementsRepository.findByIdTypologieEvenements(newCorrespondenceId);
                // Si la typologie n'existe pas, lance une exception
                if (typologie == null) {
                    throw new NotFoundException("La typologie n'existe pas");
                }

                // Crée une nouvelle correspondance
                CorrespondreEntity newCorrespondence = new CorrespondreEntity();
                newCorrespondence.setTypologieEvenements(typologie);

                // Crée un nouvel ID pour la correspondance
                IdCorrespondre idCorrespondre = new IdCorrespondre();
                idCorrespondre.setIdEvenement(evenement.getIdEvenement());
                idCorrespondre.setIdTypologieEvenements(typologie.getIdTypologieEvenements());
                newCorrespondence.setId(idCorrespondre);

                // Associe la correspondance à l'événement
                newCorrespondence.setEvenementEntity(evenement);
                // Sauvegarde la nouvelle correspondance
                correspondreRepository.saveAndFlush(newCorrespondence);
            }
            evenementRepository.flush();

            // Si tout s'est bien passé, renvoie un message de succès
            return "L'événement a été mis à jour avec succès";

            // Gestion des exceptions
        } catch (NotFoundException e) {
            e.printStackTrace();
            return "Une erreur s'est produite lors de la mise à jour de l'événement : " + e.getMessage();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return "Une erreur s'est produite lors de la mise à jour de l'événement : " + e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return "Une erreur inattendue s'est produite lors de la mise à jour de l'événement";
        }
    }

    @Transactional
    public String deleteFavoris(int idEvenement, int idUtilisateur) throws NotFoundException, UnauthorizedException {
        try {
            IdAjouterFavoris idAjouterFavoris = new IdAjouterFavoris(idUtilisateur, idEvenement);
            logger.info("Deleting favorite with idEvenement: {} and idUtilisateur: {}", idEvenement, idUtilisateur);

            AjouterFavorisEntity favoris = (AjouterFavorisEntity) ajouterFavorisRepository.findById(idAjouterFavoris)
                    .orElseThrow(() -> new NotFoundException("Favoris non trouvé"));

            if (favoris.getUtilisateurEntity().getIdUtilisateur() != idUtilisateur) {
                throw new UnauthorizedException("Seul le créateur de l'événement peut supprimer ce favoris");
            }

            ajouterFavorisRepository.deleteById(idAjouterFavoris);
            logger.info("Favorite successfully deleted");
        } catch (EmptyResultDataAccessException e) {
            logger.error("Error occurred while deleting favorite", e);
        }
        return null;
    }

    @Transactional
    public List<AjouterFavorisDto> getFavorisById(int idUtilisateur) throws NotFoundException {
        try {
            List<AjouterFavorisEntity> ajouterFavori =
                    ajouterFavorisRepository.findAllByUtilisateurEntity_IdUtilisateur(idUtilisateur);

            List<AjouterFavorisDto> favorisDtoList = new ArrayList<>();

            for (AjouterFavorisEntity ajouterFavoris : ajouterFavori) {
                AjouterFavorisDto favorisDto = new AjouterFavorisDto();
                BeanUtils.copyProperties(ajouterFavoris.getEvenementEntity(), favorisDto);

                if (ajouterFavoris.getEvenementEntity() != null) {
                    UtilisateurDto utilisateurDto = modelMapper.map(ajouterFavoris.getEvenementEntity().getUtilisateurEntity(), UtilisateurDto.class);
                    BeanUtils.copyProperties(ajouterFavoris.getEvenementEntity().getUtilisateurEntity(), utilisateurDto);

                    List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(ajouterFavoris.getEvenementEntity().getUtilisateurEntity().getIdUtilisateur());

                    List<RoleDto> rolesDto = attributions.stream()
                            .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                            .collect(Collectors.toList());

                    utilisateurDto.setRoles(rolesDto);
                    favorisDto.setUtilisateur(utilisateurDto);
                }

                LieuDto lieuDto = modelMapper.map(ajouterFavoris.getLieuEntity(), LieuDto.class);
                BeanUtils.copyProperties(ajouterFavoris.getLieuEntity(), lieuDto);
                favorisDto.setLieu(lieuDto);

                // Utiliser la relation @OneToMany pour obtenir les correspondreEntities
                List<CorrespondreEntity> correspondreEntities = ajouterFavoris.getCorrespondreEntities();

                List<TypologieEvenementsDto> typologieEvenementsDtoList = correspondreEntities.stream()
                        .map(correspondreEntity -> modelMapper.map(correspondreEntity.getTypologieEvenements(), TypologieEvenementsDto.class))
                        .collect(Collectors.toList());

                favorisDto.setIdEvenement(ajouterFavoris.getEvenementEntity().getIdEvenement());
                favorisDto.setIdUtilisateur(ajouterFavoris.getUtilisateurEntity().getIdUtilisateur());

                favorisDto.setTypologieEvenements(typologieEvenementsDtoList);

                favorisDto.setDateValidation(ajouterFavoris.getEvenementEntity().getDateValidation());

                favorisDtoList.add(favorisDto);
            }

            return favorisDtoList;

        } catch (Exception e) {
            e.printStackTrace();
            throw new NotFoundException("Aucun favoris trouvé");
        }
    }

    @Transactional
    public String deleteEvenement(int idEvenement) {
        try {
            Optional<EvenementEntity> optionalEvenementEntity = evenementRepository.findById(idEvenement);
            if (optionalEvenementEntity.isPresent()) {
                EvenementEntity evenement = optionalEvenementEntity.get();

                List<InscriptionsEntity> inscriptionsEntities = inscriptionsRepository.findByEvenementEntity(evenement);
                for (InscriptionsEntity inscription : inscriptionsEntities) {
                    inscription.setEvenementEntity(null);  // remove the reference to the event
                    inscriptionsRepository.save(inscription);  // save the updated entity
                }
                entityManager.flush();

                // Supprimer les lignes de la table AjouterFavorisEntity
                List<AjouterFavorisEntity> ajouterFavorisEntities = ajouterFavorisRepository.findByEvenementEntity(evenement);
                ajouterFavorisRepository.deleteAll(ajouterFavorisEntities);
                entityManager.flush();  // Ajoutez cette ligne

                // Supprimer les lignes de la table CorrespondreEntity
                List<CorrespondreEntity> correspondreEntities = correspondreRepository.findByEvenementEntity(evenement);
                correspondreRepository.deleteAll(correspondreEntities);
                entityManager.flush();  // Ajoutez cette ligne

                // Delete the EvenementEntity
                evenementRepository.delete(evenement);
                return "Evenement supprimé avec succès";
            } else {
                return "L'événement n'existe pas";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Une erreur s'est produite lors de la suppression de l'événement";
        }
    }
}


















