package com.mgen.amie.service;

import com.mgen.amie.auth.AuthenticationResponse;
import com.mgen.amie.entity.UtilisateurEntity;
import com.mgen.amie.repository.UtilisateurRepository;
import com.mgen.amie.request.AuthenticationRequest;
import com.mgen.amie.request.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthentificationService {
    private final UtilisateurRepository utilisateurRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    public AuthenticationResponse register(RegisterRequest request) {
        var user = UtilisateurEntity.builder()
                .mail(request.getMail())
                .nom(request.getNom())
                .prenom(request.getPrenom())
                .motDePasse(passwordEncoder.encode(request.getMotDePasse()))
                .dateDerniereConnexion(LocalDateTime.now())
                .dateDerniereInitialisationMotDePasse(LocalDateTime.now())
                .build();

        utilisateurRepository.save(user);

        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getMail(),
                        request.getMotDePasse()
                )
        );
        var user = utilisateurRepository.findByMail(request.getMail())
                .orElseThrow(() -> new RuntimeException("Utilisateur non trouvé"));

        var jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
