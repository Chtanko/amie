package com.mgen.amie.service;

import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.entity.RoleEntity;
import com.mgen.amie.model.Role;
import com.mgen.amie.repository.RoleRepository;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    private static final Logger log = LoggerFactory.getLogger(FAQService.class);

    public List<Role> getAllRoles() {
        try {
            List<RoleEntity> roles = roleRepository.findAll();
            List<Role> customRoles = new ArrayList<>();
            roles.stream().forEach(r -> {
                Role role = new Role();
                role.setIdRole(r.getIdRole());
                role.setLibelle(r.getLibelle());
                customRoles.add(role);
            });
            return customRoles;
        } catch (Exception e) {
            log.error("Erreur de récupération de roles : ", e);
            return Collections.emptyList();
        }
    }


    public String addRole(RoleEntity role) {
        try {
            roleRepository.save(role);
            return "Role ajouté";
        } catch (Exception e) {
            return "Erreur de ajout: " + e.getMessage();
        }
    }

    @Transactional
    public String updateRole(String libelle, RoleDto roleDto) throws NotFoundException {
        Optional<RoleEntity> optionalRoleEntity = roleRepository.findByLibelle(libelle);
        if (optionalRoleEntity.isPresent()) {
            RoleEntity roleEntity = optionalRoleEntity.get();
            roleEntity.setLibelle(roleDto.getLibelle());
            roleRepository.save(roleEntity);
            return "Rôle modifié avec succès";
        } else {
            throw new NotFoundException("Rôle non trouvé");
        }
    }

    public String deleteRole(int idRole) {
        try {
            Optional<RoleEntity> optionalRoleEntity = roleRepository.findById(idRole);
            if(optionalRoleEntity.isPresent()) {
                RoleEntity roleEntity = optionalRoleEntity.get();
                roleRepository.delete(roleEntity);
                return "Rôle supprimé avec succès";
            } else {
                return "Rôle n'existe pas";
            }
        } catch (Exception e) {
            return "Erreur de suppression: " + e.getMessage();
        }
    }
}
