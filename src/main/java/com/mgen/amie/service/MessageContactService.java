package com.mgen.amie.service;

import com.mgen.amie.dto.message.GestionMessageContactDto;
import com.mgen.amie.dto.message.MessageContactDto;
import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import com.mgen.amie.entity.AttribuerEntity;
import com.mgen.amie.entity.MessageContactEntity;
import com.mgen.amie.entity.UtilisateurEntity;
import com.mgen.amie.repository.AttribuerRepository;
import com.mgen.amie.repository.MessageContactRepository;
import com.mgen.amie.repository.UtilisateurRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class MessageContactService {

    @Autowired
    private MessageContactRepository messageContactRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AttribuerRepository attribuerRepository;

    @Transactional
    public MessageContactDto redigerMessage(int idUtilisateur, MessageContactDto messageDto) {
        // Créer une nouvelle entité MessageContactEntity à partir du MessageContactDto
        MessageContactEntity messageEntity = new MessageContactEntity();
        messageEntity.setObjet(messageDto.getObjet());
        messageEntity.setDescription(messageDto.getDescription());

        // Récupérer l'entité utilisateur correspondante à partir de l'ID de l'expéditeur
        UtilisateurEntity utilisateurEntity = utilisateurRepository.findById(idUtilisateur)
                .orElseThrow(() -> new EntityNotFoundException("Utilisateur non trouvé avec l'ID " + idUtilisateur));

        // Définir l'expéditeur, le destinataire et la date de rédaction pour le message
        messageEntity.setUtilisateurEntity(utilisateurEntity);
        messageEntity.setObjet(messageDto.getObjet());
        messageEntity.setDescription(messageDto.getDescription());
        messageEntity.setDateRedaction(LocalDateTime.now());

        // Enregistrer le message
        MessageContactEntity savedMessageContact = messageContactRepository.save(messageEntity);

        // Créer un nouveau MessageContactDto à partir des champs d'objet de l'entité MessageContactEntity
        MessageContactDto savedMessageDto = new MessageContactDto();
        BeanUtils.copyProperties(savedMessageContact, savedMessageDto);

        return savedMessageDto;
    }

    @Transactional
    public List<GestionMessageContactDto> getAllMessages() {

        List<UtilisateurEntity> utilisateurs = utilisateurRepository.findAll();

        List<MessageContactEntity> messagesContactEntities = messageContactRepository.findAll();

        List<GestionMessageContactDto> customMessages = new ArrayList<>();

        for (MessageContactEntity messagesContactEntity : messagesContactEntities) {
            GestionMessageContactDto gestionMessageContactDto = new GestionMessageContactDto();
            BeanUtils.copyProperties(messagesContactEntity, gestionMessageContactDto);

            if (messagesContactEntity.getUtilisateurEntity() != null) {
                UtilisateurDto utilisateurDto = modelMapper.map(messagesContactEntity.getUtilisateurEntity(), UtilisateurDto.class);

                List<AttribuerEntity> attributions =
                        attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(
                                messagesContactEntity.getUtilisateurEntity().getIdUtilisateur());
                List<RoleDto> rolesDto = new ArrayList<>();

                for (AttribuerEntity attribution : attributions) {

                    // Création d'un DTO pour le rôle associé à l'attribution en cours de la boucle
                    RoleDto roleDto = new RoleDto();
                    // Copie des propriétés du rôle de l'attribution dans le DTO
                    BeanUtils.copyProperties(attribution.getRole(), roleDto);
                    // Ajout du DTO du rôle associé à l'utilisateur en cours de la boucle dans la liste de DTO des rôles associés
                    rolesDto.add(roleDto);
                }
                // Ajout de la liste de DTO des rôles associés à l'utilisateur en cours de la boucle dans le DTO de l'utilisateur
                utilisateurDto.setRoles(rolesDto);
                // Ajout du DTO de l'utilisateur en cours de la boucle dans la liste de DTO des utilisateurs avec leurs rôles
                gestionMessageContactDto.setUtilisateur(Arrays.asList(utilisateurDto));
            }
            gestionMessageContactDto.setIdMessageContact(messagesContactEntity.getIdMessageContact());
            customMessages.add(gestionMessageContactDto);
        }
        return customMessages;
    }

    @Transactional
    public List<MessageContactDto> getMessagesByUtilisateurId(int idUtilisateur) {
        List<MessageContactEntity> messageContactEntities = messageContactRepository.findByUtilisateurEntity_IdUtilisateur(idUtilisateur);
        List<MessageContactDto> messageContactDTOs = new ArrayList<>();
        for (MessageContactEntity messageContactEntity : messageContactEntities) {
            if (messageContactEntity.getUtilisateurEntity().getIdUtilisateur() == idUtilisateur) {
                MessageContactDto messageContactDTO = new MessageContactDto();
                BeanUtils.copyProperties(messageContactEntity, messageContactDTO);
                messageContactDTOs.add(messageContactDTO);
            }
        }
        return messageContactDTOs;
    }

    @Transactional


    public String deleteMessageByIdUtilisateur(int idMessageContact) {
        try {
            Optional<MessageContactEntity> optionalMessageContactEntity =
                    messageContactRepository.findById(idMessageContact);
            if (optionalMessageContactEntity.isPresent()) {
                MessageContactEntity messageContactEntity =
                        optionalMessageContactEntity.get();
                messageContactRepository.delete(messageContactEntity);
            }
            return "Message supprimé";
        } catch (Exception e) {
            return "Erreur de suppression : " + e.getMessage();
        }

    }
}
