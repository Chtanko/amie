package com.mgen.amie.service;

import com.mgen.amie.composite.IdAttribuer;
import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.dto.utilisateur.UtilisateurAjoutDto;
import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import com.mgen.amie.dto.utilisateur.UtilisateurModificationDto;
import com.mgen.amie.entity.AttribuerEntity;
import com.mgen.amie.entity.EvenementEntity;
import com.mgen.amie.entity.RoleEntity;
import com.mgen.amie.entity.UtilisateurEntity;
import com.mgen.amie.model.Utilisateur;
import com.mgen.amie.repository.AttribuerRepository;
import com.mgen.amie.repository.EvenementRepository;
import com.mgen.amie.repository.RoleRepository;
import com.mgen.amie.repository.UtilisateurRepository;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UtilisateurService {

    @Autowired
    UtilisateurRepository utilisateurRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AttribuerRepository attribuerRepository;
    @Autowired
    private EvenementRepository evenementRepository;

    public UtilisateurDto getUtilisateurAvecRoles(int idUtilisateur) throws NotFoundException {
        UtilisateurEntity utilisateurEntity =
                utilisateurRepository.findByIdUtilisateur(idUtilisateur)
                .orElseThrow(() -> new NotFoundException("Utilisateur non trouvé"));

        List<AttribuerEntity> attributions =
                attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(idUtilisateur);

        // mapper les entités vers un DTO
        UtilisateurDto utilisateurDto = modelMapper.map(utilisateurEntity, UtilisateurDto.class);
        List<RoleDto> rolesDto = attributions.stream()
                .map(attribution -> modelMapper.map(attribution.getRole(), RoleDto.class))
                .collect(Collectors.toList());
        utilisateurDto.setRoles(rolesDto);

        return utilisateurDto;
    }

    public List<UtilisateurDto> getAllUtilisateursAvecRoles() throws NotFoundException {
        // Récupération de la liste des utilisateurs depuis la base de données
        List<UtilisateurEntity> utilisateurEntityList = utilisateurRepository.findAll();
        // Récupération de la liste des attributions de rôles depuis la base de données
        List<AttribuerEntity> attribuerEntityList = attribuerRepository.findAll();
        // Initialisation d'une liste vide pour stocker les DTO des utilisateurs avec leurs rôles
        List<UtilisateurDto> utilisateurDtoList = new ArrayList<>();

        // Boucle pour chaque utilisateur de la liste
        for (UtilisateurEntity utilisateurEntity : utilisateurEntityList) {
            // Création d'un DTO pour l'utilisateur en cours de la boucle
            UtilisateurDto utilisateurDto = modelMapper.map(utilisateurEntity, UtilisateurDto.class);
            // Initialisation d'une liste vide pour stocker les DTO des rôles associés à l'utilisateur en cours de la boucle
            List<RoleDto> rolesDtoList = new ArrayList<>();

            // Boucle pour chaque attribution de rôle de la liste
            for (AttribuerEntity attribuerEntity : attribuerEntityList) {
                // Si l'id de l'utilisateur de l'attribution est égal à l'id de l'utilisateur en cours de la boucle
                //ou Vérification si l'utilisateur en cours de la boucle a le rôle associé à l'attribution de rôle en cours de la boucle
                if (attribuerEntity.getUtilisateur().getIdUtilisateur() == utilisateurEntity.getIdUtilisateur()) {
                    // Ajouter le rôle de l'attribution à la liste des rôles de l'utilisateur en cours de la boucle
                    // Ajout du DTO du rôle associé à l'utilisateur en cours de la boucle dans la liste de DTO des rôles associés
                    rolesDtoList.add(modelMapper.map(attribuerEntity.getRole(), RoleDto.class));
                }
            }
            // Ajout de la liste de DTO des rôles associés à l'utilisateur
            // en cours de la boucle dans le DTO de l'utilisateur en cours de la boucle
            utilisateurDto.setRoles(rolesDtoList);
            // Ajout du DTO de l'utilisateur en cours de la boucle dans la liste de DTO des utilisateurs avec leurs rôles
            utilisateurDtoList.add(utilisateurDto);
        }
        // Retour de la liste de DTO des utilisateurs avec leurs rôles
        return utilisateurDtoList;
    }

    // @Transactional : toutes les opérations de base de données effectuées
    // dans cette méthode seront soit toutes validées,
    // soit toutes annulées en cas d'erreur.
    @Transactional
    public void assignerRoleAUtilisateur(int idUtilisateur, int idRole) throws NotFoundException {
        // récupérer l'utilisateur associé à l'id d'utilisateur
        //utilisateurRepository et roleRepository sont des objets injectés via l'injection de dépendances de Spring
        UtilisateurEntity utilisateur = utilisateurRepository.findByIdUtilisateur(idUtilisateur)
                .orElseThrow(() -> new NotFoundException("Utilisateur non trouvé"));

        // récupérer le rôle associé à l'id de rôle
        RoleEntity role = roleRepository.findById(idRole)
                .orElseThrow(() -> new NotFoundException("Rôle non trouvé"));

        // créer un objet IdAttribuer avec les IDs de l'utilisateur et du rôle
        IdAttribuer idAttribuer = new IdAttribuer(idRole, idUtilisateur);

        // créer une nouvelle entité AttribuerEntity
        AttribuerEntity attribution = new AttribuerEntity();
        attribution.setId(idAttribuer); // associer l'objet IdAttribuer à l'attribution
        attribution.setUtilisateur(utilisateur);// associer l'utilisateur à l'attribution
        attribution.setRole(role);// associer le rôle à l'attribution
        attribution.setDateAttribution(LocalDateTime.now());// définir la date d'attribution de l'attribution

        // sauvegarder l'entité AttribuerEntity dans la base de données
        attribuerRepository.save(attribution);
    }

    public List<Utilisateur> getAllUtilisateurs() {
        try {
            List<UtilisateurEntity> utilisateursEntities = (List<UtilisateurEntity>) utilisateurRepository.findAll();
            List<Utilisateur> utilisateurs = new ArrayList<>();
            utilisateursEntities.stream().forEach(e -> {
                Utilisateur utilisateur = new Utilisateur();
                BeanUtils.copyProperties(e, utilisateur);
                utilisateurs.add(utilisateur);
            });
            return utilisateurs;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public UtilisateurEntity addUtilisateur(UtilisateurAjoutDto utilisateurAjoutDto) throws NotFoundException {
        UtilisateurEntity utilisateur = new UtilisateurEntity();

        utilisateur.setNom(utilisateurAjoutDto.getNom());
        utilisateur.setPrenom(utilisateurAjoutDto.getPrenom());
        utilisateur.setMail(utilisateurAjoutDto.getMail());
        utilisateur.setMotDePasse(utilisateurAjoutDto.getMotDePasse());
        utilisateur.setDateDerniereInitialisationMotDePasse(LocalDateTime.now());
        utilisateur.setDateDerniereConnexion(LocalDateTime.now());
        utilisateurRepository.save(utilisateur);
        return utilisateur;
    }

    public String updateUtilisateur(int idUtilisateur, UtilisateurModificationDto utilisateur) throws NotFoundException {
        // utilise l'interface CrudRepository de Spring Data pour chercher un utilisateur avec l'ID donné dans la base de données.
        // La méthode findById renvoie un objet Optional qui peut contenir
        // l'utilisateur si celui-ci existe, ou être vide si l'utilisateur n'existe pas
        Optional<UtilisateurEntity> optionalUtilisateurEntity = utilisateurRepository.findById(idUtilisateur);

        if(optionalUtilisateurEntity.isPresent()) {
            //extrait l'objet UtilisateurEntity contenu dans l'objet Optional
            // retourné par la méthode findById du repository de l'utilisateur
            UtilisateurEntity utilisateurEntity = optionalUtilisateurEntity.get();
            utilisateurEntity.setMail(utilisateur.getMail());
            utilisateurEntity.setNom(utilisateur.getNom());
            utilisateurEntity.setPrenom(utilisateur.getPrenom());
            utilisateurEntity.setMotDePasse(utilisateur.getMotDePasse());
            utilisateurEntity.setDateDerniereInitialisationMotDePasse(LocalDateTime.now());
            utilisateurEntity.setDateDerniereConnexion(LocalDateTime.now());
            utilisateurRepository.save(utilisateurEntity);
            return "Utilisateur modifié avec succès";
        } else {
            return "Utilisateur n'existe pas";
        }
    }

    public String deleteUtilisateur(int idUtilisateur) {
        try {
            // Essayer de trouver l'utilisateur par son ID
            Optional<UtilisateurEntity> optionalUtilisateurEntity = utilisateurRepository.findById(idUtilisateur);
            if(optionalUtilisateurEntity.isPresent()) {
                // Si l'utilisateur est trouvé, obtenir l'entité utilisateur
                UtilisateurEntity utilisateurEntity = optionalUtilisateurEntity.get();

                // Vérifier s'il y a encore des événements qui référencent cet utilisateur
                List<EvenementEntity> events = evenementRepository.findByUtilisateurEntity_IdUtilisateur(idUtilisateur);
                if (!events.isEmpty()) {
                    // S'il y en a, retourner un message d'erreur avec les ID des événements
                    return "Impossible de supprimer l'utilisateur car il est toujours référencé dans les événements suivants : "
                            + " " + events.stream().map(EvenementEntity::getIdEvenement).toList();
                }

                // Trouver toutes les attributions de rôles pour cet utilisateur
                List<AttribuerEntity> attributions = attribuerRepository.findAllByUtilisateurEntity_IdUtilisateur(idUtilisateur);
                for (AttribuerEntity attribution : attributions) {
                    // Supprimer chaque attribution de rôle
                    attribuerRepository.delete(attribution);
                }

                // Maintenant que toutes les références à l'utilisateur ont été supprimées, supprimer l'utilisateur
                utilisateurRepository.delete(utilisateurEntity);
                return "Utilisateur supprimé avec succès";
            } else {
                // Si l'utilisateur n'est pas trouvé, retourner un message d'erreur
                return "Utilisateur n'existe pas";
            }
        } catch (Exception e) {
            // Si une autre erreur se produit, retourner un message d'erreur avec les détails de l'exception
            return "Erreur de suppression: " + e.getMessage();
        }
    }

}