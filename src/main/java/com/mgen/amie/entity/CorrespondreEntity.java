package com.mgen.amie.entity;

import com.mgen.amie.composite.*;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Correspondre")
@Table(name = "correspondre")
public class CorrespondreEntity {

    @EmbeddedId
    private IdCorrespondre id = new IdCorrespondre();

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @MapsId("idEvenement")
    @JoinColumn(
            name = "evenement_id",
            foreignKey = @ForeignKey(
                    name = "correspondre_evenement_id_fk"
            )
    )
    private EvenementEntity evenementEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @MapsId("idTypologieEvenements")
    @JoinColumn(
            name = "typologie_evenements_id",
            referencedColumnName = "idTypologieEvenements",
            foreignKey = @ForeignKey(
                    name = "correspondre_typologie_evenements_id_fk"
            )
    )
    private TypologieEvenementsEntity typologieEvenements;

    public CorrespondreEntity(
            EvenementEntity evenement,
            TypologieEvenementsEntity typologieEvenementsEntity) {
        this.evenementEntity = evenement;
        this.typologieEvenements = typologieEvenementsEntity;
    }

    public void setTypologieEvenements(TypologieEvenementsEntity typologie) {
        this.typologieEvenements = typologie;
    }

    public void setEvenement(EvenementEntity evenement) {
        this.evenementEntity = evenement;
    }

    public TypologieEvenementsEntity getTypologieEvenements() {
        return this.typologieEvenements;
    }

}