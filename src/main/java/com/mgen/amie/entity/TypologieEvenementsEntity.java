package com.mgen.amie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "TypologieEvenements")
@Table(name = "typologieevenements")
public class TypologieEvenementsEntity {
    @Id
    @SequenceGenerator(
            name = "typologie_evenements_sequence",
            sequenceName = "typologie_evenements_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "typologie_evenements_sequence"
    )
    @Column(
            name = "idTypologieEvenements",
            updatable = true,
            unique=true
    )
    private int idTypologieEvenements;

    @Column(
            name = "label"
    )
    private String label;

    @OneToMany(
            mappedBy = "typologieEvenements",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<CorrespondreEntity> correspondreEntities = new ArrayList<>();
}
