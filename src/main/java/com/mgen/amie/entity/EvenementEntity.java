package com.mgen.amie.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Evenement")
@Table(name = "evenement")
public class EvenementEntity {

    @Id
    @SequenceGenerator(
            name = "evenement_sequence",
            sequenceName = "evenement_sequence",
            allocationSize =  1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "evenement_sequence"
    )
    @Column(
            name = "idEvenement",
            updatable = false
    )
    private int idEvenement;

    @Column(
            name = "label",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String label;

    @Column(
            name = "description",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String description;

    @Column(
            name = "date_debut",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String date_debut;

    @Column(
            name = "date_fin",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String date_fin;

    @Column(
            name = "date_creation_evenement",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateCreationEvenement;

    @Column(
            name = "heure_debut",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String heure_debut;

    @Column(
            name = "heure_fin",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String heure_fin;

    @Column(
            name = "lien_replay",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String lien_replay;

    @Column(
            name = "lien_ressources",
            nullable = true,
            columnDefinition = "TEXT"
    )
    private String lien_ressources;

    @Column(name = "image",
            columnDefinition = "BYTEA")
    private byte[] image;


    @Column(name = "date_validation",
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    private LocalDateTime dateValidation;

    @Column(
            name = "statut",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String statut;

    @JsonManagedReference
    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.MERGE}
    )
    @JoinColumn(
            name = "lieu_id",
            nullable = false,
            referencedColumnName = "idLieu",
            foreignKey = @ForeignKey(
                    name= "evenement_lieu_fk"
            )
    )
    private LieuEntity lieuEntity;

    @ManyToOne
    @JoinColumn(
            name = "utilisateur_id",
            nullable = true,
            referencedColumnName = "idUtilisateur",
            foreignKey = @ForeignKey(
                    name = "evenement_utilisateur_fk"
            )
    )
    private UtilisateurEntity utilisateurEntity;

    @OneToMany(mappedBy = "evenementEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CorrespondreEntity> correspondreEntities;

    @OneToMany(mappedBy = "evenementEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InscriptionsEntity> inscriptions = new ArrayList<>();

    @OneToMany(mappedBy = "evenementEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AjouterFavorisEntity> ajouterFavorisEntities = new ArrayList<>();

    public List<InscriptionsEntity> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(List<InscriptionsEntity> inscriptions) {
        this.inscriptions = inscriptions;
    }


    // Ajoutez les getters et setters pour correspondreEntities ici
    public List<CorrespondreEntity> getCorrespondreEntities() {
        return correspondreEntities;
    }

    public void setCorrespondreEntities(List<CorrespondreEntity> correspondreEntities) {
        this.correspondreEntities = correspondreEntities;
    }

    public void setLieu(LieuEntity lieu) {
        this.lieuEntity = lieu;

    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "EvenementEntity{" +
                "idEvenement=" + idEvenement +
                ", label='" + label + '\'' +
                ", description='" + description + '\'' +
                ", date_debut='" + date_debut + '\'' +
                ", date_fin='" + date_fin + '\'' +
                ", dateCreationEvenement=" + dateCreationEvenement +
                ", heure_debut='" + heure_debut + '\'' +
                ", heure_fin='" + heure_fin + '\'' +
                ", lien_replay='" + lien_replay + '\'' +
                ", lien_ressources='" + lien_ressources + '\'' +
                ", image=" + Arrays.toString(image) +
                ", dateValidation=" + dateValidation +
                ", statut='" + statut + '\'' +
                '}';
    }

}
