package com.mgen.amie.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Utilisateur")
@Table(
        name = "utilisateur",
        uniqueConstraints = {
                @UniqueConstraint(name = "utilisateur_mail_unique",
                        columnNames = "mail")
        }
)
public class UtilisateurEntity implements UserDetails {

    @Id
    @SequenceGenerator(
            name = "utilisateur_sequence",
            sequenceName = "utilisateur_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "utilisateur_sequence"
    )
    @Column(
            name = "idUtilisateur",
            updatable = false
    )
    private int idUtilisateur;

    @Column(
            name= "prenom",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String prenom;

    @Column(
            name= "nom",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String nom;

    @Column(
            name= "mail",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String mail;

    @Column(
            name= "mot_de_passe",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String motDePasse;

    @Column(
            name= "date_derniere_initialisation_mot_de_passe",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateDerniereInitialisationMotDePasse;

    @Column(
            name= "date_derniere_connexion",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateDerniereConnexion;

    @OneToMany(
            mappedBy = "utilisateurEntity",
            orphanRemoval = true,
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
            fetch = FetchType.LAZY
    )
    @JsonManagedReference
    private final List<MessageContactEntity> messagesContact = new ArrayList<>();


    @OneToMany(mappedBy = "utilisateurEntity", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private List<InscriptionsEntity> inscriptions = new ArrayList<>();

    @OneToMany(mappedBy = "utilisateurEntity", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private List<AttribuerEntity> attributionsEntities;

    public UtilisateurEntity(String prenom, String nom, String mail, String motDePasse) {
        this.prenom = prenom;
        this.nom = nom;
        this.mail = mail;
        this.motDePasse = motDePasse;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("ROLE_USER"));
    }

    public List<AttribuerEntity> getAttributionsEntities() {
        return attributionsEntities;
    }

    public void setAttributionsEntities(List<AttribuerEntity> attributionsEntities) {
        this.attributionsEntities = attributionsEntities;
    }

    @Override
    public String getPassword() {
        return motDePasse;
    }

    @Override
    public String getUsername() {
        return mail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
