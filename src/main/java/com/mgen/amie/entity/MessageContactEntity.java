package com.mgen.amie.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.SEQUENCE;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "MessageContact")
@Table(name = "messagecontact")
public class MessageContactEntity {

    @Id
    @SequenceGenerator(
            name = "message_contact",
            sequenceName =  "message_contact_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "message_contact_sequence"
    )
    @Column(
            name = "idMessageContact",
            updatable = false
    )
    private int idMessageContact;

    @Column(
            name = "objet",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String objet;

    @Column(
            name = "description",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String description;

    @Column(
            name= "date_redaction",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateRedaction;

    @ManyToOne
    @JoinColumn(
            name = "utilisateur_id",
            nullable = false,
            referencedColumnName = "idUtilisateur",
            foreignKey = @ForeignKey(
                    name = "utilisateur_message_contact_fk"
            )
    )
    @JsonBackReference
    private UtilisateurEntity utilisateurEntity;

}
