package com.mgen.amie.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Role")
@Table(name = "role")
public class RoleEntity {

    @Id
    @SequenceGenerator(
            name = "role_sequence",
            sequenceName = "role_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "role_sequence"
    )
    @Column(
            name = "idRole"
    )
    private int idRole;

    @Column(
            name = "libelle",
            columnDefinition = "TEXT"
    )
    private String libelle;

    @OneToMany(mappedBy = "roleEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AttribuerEntity> attribuerEntities = new ArrayList<>();

}
