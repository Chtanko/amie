package com.mgen.amie.entity;

import com.mgen.amie.composite.IdAttribuer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Attribuer")
@Table(name = "attribuer")
public class AttribuerEntity {

    @EmbeddedId
    private IdAttribuer id;

    @ManyToOne
    @MapsId("idUtilisateur")
    @JoinColumn(
            name = "utilisateur_id",
            foreignKey = @ForeignKey(
                    name = "attribuer_utilisateur_id_fk"
            )
    )
    private UtilisateurEntity utilisateurEntity;

    @ManyToOne
    @MapsId("idRole")
    @JoinColumn(
            name = "role_id",
            foreignKey = @ForeignKey(
                    name = "attribuer_role_id_fk"
            )
    )
    private RoleEntity roleEntity;

    @Column(
            name = "date_attribution",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateAttribution;


    public IdAttribuer getId() {
        return id;
    }

    public void setId(IdAttribuer id) {
        this.id = id;
    }

    public UtilisateurEntity getUtilisateur() {
        return utilisateurEntity;
    }

    public void setUtilisateur(UtilisateurEntity utilisateurEntity) {
        this.utilisateurEntity = utilisateurEntity;
    }

    public RoleEntity getRole() {
        return roleEntity;
    }

    public void setRole(RoleEntity roleEntity) {
        this.roleEntity = roleEntity;
    }

    public void setDateAttribution(LocalDateTime dateAttribution) {
        this.dateAttribution = dateAttribution;
    }


}
