package com.mgen.amie.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Lieu")
@Table(name = "lieu")
public class LieuEntity {

    @Id
    @SequenceGenerator(
            name = "lieu_sequence",
            sequenceName = "lieu_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "lieu_sequence"
    )
    @Column(
            name = "idLieu"
    )
    private int idLieu;

    @Column(
            name = "coordonees_gps",
            columnDefinition = "TEXT"
    )
    private String coordoneesGps;

    @Column(
            name = "adresse",
            columnDefinition = "TEXT"
    )
    private String adresse;

    @Column(
            name = "ville",
            columnDefinition = "TEXT"
    )
    private String ville;

    @Column(
            name = "code_postal",
            columnDefinition = "TEXT"
    )
    private String codePostal;

    @Column(
            name = "localisation",
            columnDefinition = "TEXT"
    )
    private String localisation;

    @Column(
            name = "places",
            columnDefinition = "INTEGER"
    )
    private int places;

    @JsonBackReference
    @OneToMany(
            mappedBy = "lieuEntity",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JsonIgnoreProperties("lieuEntity")
    private List<EvenementEntity> evenements = new ArrayList<>();

    @Override
    public String toString() {
        return "LieuEntity{" +
                "idLieu=" + idLieu +
                ", coordoneesGps='" + coordoneesGps + '\'' +
                ", adresse='" + adresse + '\'' +
                ", ville='" + ville + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", localisation='" + localisation + '\'' +
                ", places=" + places +
                '}';
    }

}
