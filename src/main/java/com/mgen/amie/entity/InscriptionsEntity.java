package com.mgen.amie.entity;

import com.mgen.amie.composite.IdInscriptions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Inscriptions")
@Table(name = "inscriptions")
public class InscriptionsEntity {

    @EmbeddedId
    private IdInscriptions id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @MapsId("idUtilisateur")
    @JoinColumn(
            name = "utilisateur_id",
            foreignKey = @ForeignKey(
                    name = "inscriptions_utilisateur_id_fk"
            )
    )
    private UtilisateurEntity utilisateurEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @MapsId("idEvenement")
    @JoinColumn(
            name = "evenement_id",
            foreignKey = @ForeignKey(
                    name = "inscriptions_evenement_id_fk"
            )
    )
    private EvenementEntity evenementEntity;


    @Column(
            name = "date_inscription",
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime dateInscription;

    public void setUtilisateur(UtilisateurEntity utilisateurEntity) {
        this.utilisateurEntity = utilisateurEntity;
    }

    public void setEvenement(EvenementEntity evenementEntity) {
        this.evenementEntity = evenementEntity;
    }

    public EvenementEntity getEvenement() {
        return evenementEntity;

    }

    public LieuEntity getLieuEntity() {
       return evenementEntity.getLieuEntity();
    }

    public List<CorrespondreEntity> getCorrespondreEntities() {
        return evenementEntity.getCorrespondreEntities();
    }
}
