package com.mgen.amie.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {

    private int idUtilisateur;

    private String prenom;

    private String nom;

    private String mail;

    private String motDePasse;

    private LocalDateTime dateDerniereInitialisationMotDePasse;

}
