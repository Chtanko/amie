package com.mgen.amie.model;

import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Inscriptions {

    private int idInscriptions;
    private Utilisateur utilisateur;
    private Evenement evenement;
    private LocalDateTime dateInscription;

}
