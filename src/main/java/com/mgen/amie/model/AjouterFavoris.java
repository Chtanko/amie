package com.mgen.amie.model;

import com.mgen.amie.composite.IdAjouterFavoris;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AjouterFavoris {

    private IdAjouterFavoris id;
    private Utilisateur utilisateur;
    private Evenement evenement;
    private LocalDateTime dateAjout;

}