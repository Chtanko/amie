package com.mgen.amie.model;

import java.util.Objects;

public class TypologieEvenements {

    private int idTypologieEvenements;
    private String label;

    public TypologieEvenements() {
    }

    public int getIdTypologieEvenements() {
        return idTypologieEvenements;
    }

    public void setIdTypologieEvenements(int idTypologieEvenements) {
        this.idTypologieEvenements = idTypologieEvenements;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypologieEvenements that = (TypologieEvenements) o;
        return idTypologieEvenements == that.idTypologieEvenements && Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTypologieEvenements, label);
    }
}
