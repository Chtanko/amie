package com.mgen.amie.composite;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IdAttribuer implements Serializable {

    @Column(name = "role_id")
    private int idRole;

    @Column(name = "utilisateur_id")
    private int idUtilisateur;

}
