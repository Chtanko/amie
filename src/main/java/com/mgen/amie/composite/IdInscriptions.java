package com.mgen.amie.composite;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IdInscriptions implements Serializable {

    @Column(name = "id_utilisateur")
    private int idUtilisateur;

    @Column(name = "id_evenement")
    private int idEvenement;

}
