package com.mgen.amie.composite;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IdCorrespondre implements Serializable {

    @Column(name = "evenement_id")
    private int idEvenement;

    @Column(name = "typologie_evenement_id")
    private int idTypologieEvenements;
}
