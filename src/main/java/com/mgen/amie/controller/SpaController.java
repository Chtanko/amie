package com.mgen.amie.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpaController {

    //ecris le spa controller pour naviguer entre les pages
    @RequestMapping({"/", "/login", "/lieux", "/events", "/users", "/faq", "/tag", "/messages"})
    public String index() {
        return "forward:/index.html";
    }
}
