package com.mgen.amie.controller;

import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.entity.RoleEntity;
import com.mgen.amie.model.Role;
import com.mgen.amie.repository.RoleRepository;
import com.mgen.amie.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins ="*")
@PreAuthorize("hasAuthority('Administrateur')")
@SecurityRequirement(name = "Bearer Authentication")
@RequestMapping("/api/role/")
@Api(description = "Gestion des roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    RoleRepository roleRepository;

    @ApiOperation(value = "Liste des roles ", notes = "Affiche la liste des roles")
    @RequestMapping(value = "getallroles", method = RequestMethod.GET)
    public List<Role> getAllRoles(){
        return roleService.getAllRoles();
    }

    @ApiOperation(value = "Modifier un rôle", notes = "Modifier un rôle dans la base de données")
    @RequestMapping(value = "/update/{idRole}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateRole(
            @PathVariable int idRole,
            @RequestBody RoleDto roleDto) {
        try {
            RoleEntity roleEntity = roleRepository.findById(idRole)
                    .orElseThrow(() -> new NotFoundException("Rôle non trouvé"));
            roleEntity.setLibelle(roleDto.getLibelle());
            roleRepository.save(roleEntity);
            return ResponseEntity.ok("Rôle modifié avec succès");
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @ApiOperation(value = "Ajouter un role", notes = "Ajouter un role")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addRole(@Valid @RequestBody RoleDto roleDto) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setLibelle(roleDto.getLibelle());
        return roleService.addRole(roleEntity);
    }

    @ApiOperation(value = "Supprimer un role", notes = "Supprimer un role")
    @RequestMapping(value = "/delete/{idRole}", method = RequestMethod.DELETE)
    public String deleteRole(@PathVariable int idRole) {
        return roleService.deleteRole(idRole);
    }
}
