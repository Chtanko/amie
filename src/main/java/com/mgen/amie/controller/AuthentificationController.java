package com.mgen.amie.controller;

import com.mgen.amie.request.AuthenticationRequest;
import com.mgen.amie.auth.AuthenticationResponse;
import com.mgen.amie.request.RegisterRequest;
import com.mgen.amie.service.AuthentificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins ="*")
@RequestMapping("/api/authentification/")
@RequiredArgsConstructor
@Api(description = "Gestion de l'authentification")
public class AuthentificationController {

    private final AuthentificationService authService;

    @ApiOperation(value = "Inscription d'un utilisateur", notes = "Inscription d'un utilisateur dans la base de données")
    @RequestMapping(value = "inscription", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody RegisterRequest request
    ) {
        return ResponseEntity.ok(authService.register(request));
    }

    @ApiOperation(value = "Authentification d'un utilisateur", notes = "Authentification d'un utilisateur dans la base de données")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationResponse> authenticate(
            @RequestBody AuthenticationRequest request
    ) {
        AuthenticationResponse authenticationResponse = authService.authenticate(request);
        String bearerToken = "Bearer " + authenticationResponse.getToken();
        return ResponseEntity.ok(new AuthenticationResponse(bearerToken));
    }
}