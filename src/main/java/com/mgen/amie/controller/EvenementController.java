package com.mgen.amie.controller;

import com.mgen.amie.dto.ajouterfavoris.AjouterFavorisDto;
import com.mgen.amie.dto.evenement.EvenementCompletDto;
import com.mgen.amie.dto.evenement.EvenementUpdateDto;
import com.mgen.amie.dto.inscriptions.InscriptionsDto;
import com.mgen.amie.repository.EvenementRepository;
import com.mgen.amie.repository.LieuRepository;
import com.mgen.amie.repository.UtilisateurRepository;
import com.mgen.amie.request.EvenementRequest;
import com.mgen.amie.service.EvenementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/evenement/")
@PreAuthorize("hasAuthority('Administrateur') OR hasAuthority('Organisateur') OR hasAuthority('Collaborateur')")
@SecurityRequirement(name = "Bearer Authentication")
@Api(description = "Gestion des événements")
public class EvenementController {

    private final EvenementService evenementService;
    private final LieuRepository lieuRepository;
    private final EvenementRepository evenementRepository;

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public EvenementController(EvenementService evenementService,
                               LieuRepository lieuRepository,
                               EvenementRepository evenementRepository,
                               UtilisateurRepository utilisateurRepository) {
        this.evenementService = evenementService;
        this.lieuRepository = lieuRepository;
        this.evenementRepository = evenementRepository;
    }

    @ApiOperation(value = "Obtenir la liste des événements", notes = "Obtenir tous les événements enregistrés dans la base de données")
    @RequestMapping(value = "getallevenements", method = RequestMethod.GET)
    public List<EvenementCompletDto> getAllEvenements() {
        return evenementService.getAllEvenements();
    }

    @ApiOperation(value = "Ajouter en favoris", notes = "Ajouter un événement en favoris")
    @RequestMapping(value = "ajouterfavoris/{idUtilisateur}/{idEvenement}", method = RequestMethod.POST)
    public ResponseEntity<String> ajouterEnFavoris(
            @PathVariable("idUtilisateur") int idUtilisateur,
            @PathVariable("idEvenement") int idEvenement) {
        try {
            evenementService.ajouterEvenementFavoris(idUtilisateur, idEvenement);
            return ResponseEntity.ok("Ajout événement en favoris réussi");
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (IllegalStateException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ApiOperation(value = "Inscription à un événement", notes = "Inscription d'un utilisateur à un événement dans la base de données en fonction de l'ID de l'utilisateur et de l'ID de l'événement")
    @RequestMapping(value = "{idUtilisateur}/{idEvenement}", method = RequestMethod.POST)
    public ResponseEntity<String> inscriptionEvenement(
            @PathVariable int idUtilisateur,
            @PathVariable int idEvenement) {
        try {
            evenementService.inscriptionEvenement(idUtilisateur, idEvenement);
            return ResponseEntity.ok("Inscription à l'événement réussie");
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (IllegalStateException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ApiOperation(value = "Visualiser les favoris d'un utilisateur", notes = "Visualiser les favoris d'un utilisateur en fonction de son ID")
    @GetMapping(value = "getfavorisbyidutilisateur/{idUtilisateur}")
    public ResponseEntity<List<AjouterFavorisDto>> getFavorisByIdUtilisateur(@PathVariable int idUtilisateur) {
        try {
            List<AjouterFavorisDto> favoris = evenementService.getFavorisById(idUtilisateur);
            return ResponseEntity.ok(favoris);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value="Visualiser la liste des inscriptions", notes = "Visualiser la liste des inscriptions")
    @GetMapping(value = "getallinscriptions")
    public ResponseEntity<List<InscriptionsDto>> getAllInscriptions() {
        try {
            List<InscriptionsDto> inscriptions = evenementService.getAllInscriptions();
            return ResponseEntity.ok(inscriptions);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Obtenir les inscriptions d'un utilisateur", notes = "Obtenir la liste des inscriptions d'un utilisateur avec l'ID renseigné")
    @RequestMapping(value = "getinscriptions/{idUtilisateur}", method = RequestMethod.GET)
    public ResponseEntity<List<InscriptionsDto>> getInscriptionsById(@PathVariable int idUtilisateur) {
        try {
            List<InscriptionsDto> inscriptions = evenementService.getAllInscriptionsByIdUtilisateur(idUtilisateur);
            return ResponseEntity.ok(inscriptions);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Obtenir les événements d'un utilisateur", notes = "Obtenir la liste des événements créés par un utilisateur avec l'ID renseigné")
    @RequestMapping(value = "{idUtilisateur}", method = RequestMethod.GET)
    public ResponseEntity<List<EvenementCompletDto>> getEvenementsById(@PathVariable int idUtilisateur) {
        List<EvenementCompletDto> evenements = evenementService.getEvenementsByIdUtilisateur(idUtilisateur);
        return ResponseEntity.ok(evenements);
    }

    @ApiOperation(value = "Ajouter un événement", notes = "Ajouter un événement dans la base de données")
    @RequestMapping(value = "createevenement/{idUtilisateur}/{idLieu}", method = RequestMethod.POST)
    public String createEvenement(@PathVariable("idUtilisateur") int idUtilisateur,
                                  @PathVariable("idLieu")  int idLieu,
                                  @RequestBody EvenementRequest evenementRequest) {
        return evenementService.createEvenementById(idUtilisateur, idLieu, evenementRequest);
    }

    @ApiOperation(value = "Modifier un événement", notes = "Modifier un événement dans la base de données en fonction de son ID et de l'ID de l'utilisateur")
    @RequestMapping(value = "{idUtilisateur}/{idEvenement}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateEvenement(
            @PathVariable int idUtilisateur,
            @PathVariable int idEvenement,
            @RequestBody EvenementUpdateDto evenementUpdateDto) {
        try {
            String message = evenementService.updateEvenement(idUtilisateur, idEvenement, evenementUpdateDto);
            return ResponseEntity.ok(message);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ApiOperation(value = "Supprimer un événement", notes = "Supprimer un événement dans la base de données")
    @RequestMapping(value = "deleteevenement/{IdEvenement}", method = RequestMethod.DELETE)
    public String deleteEvenement(@PathVariable("IdEvenement") int idEvenement) {
        return evenementService.deleteEvenement(idEvenement);
    }

    @ApiOperation(value = "Supprimer une inscription", notes = "Supprimer une inscription dans la base de données")
    @RequestMapping(value = "deleteinscription/{idEvenement}/{idUtilisateur}", method = RequestMethod.DELETE)
    public String deleteInscription(@PathVariable("idEvenement") int idEvenement,
                                    @PathVariable("idUtilisateur") int idUtilisateur) {
        try {
            return evenementService.deleteInscription(idEvenement, idUtilisateur);
        } catch (NotFoundException e) {
            return "Inscription non trouvée";
        } catch (EvenementService.UnauthorizedException e) {
            return "Seul le créateur de l'événement peut supprimer cette inscription";
        }
    }

    @ApiOperation(value = "Supprimer un favoris", notes = "Supprimer un favoris dans la base de données")
    @RequestMapping(value = "deletefavoris/{idEvenement}/{idUtilisateur}", method = RequestMethod.DELETE)
    public String deleteFavoris(@PathVariable("idEvenement") int idEvenement,
                                @PathVariable("idUtilisateur") int idUtilisateur) {
        try {
            return evenementService.deleteFavoris(idEvenement, idUtilisateur);
        } catch (NotFoundException e) {
            return "Favoris non trouvé";
        } catch (EvenementService.UnauthorizedException e) {
            return "Seul le créateur de l'événement peut supprimer ce favoris";
        }
    }
}