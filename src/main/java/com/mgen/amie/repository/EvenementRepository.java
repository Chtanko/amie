package com.mgen.amie.repository;


import com.mgen.amie.entity.EvenementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface EvenementRepository extends JpaRepository<EvenementEntity, Integer> {
    @Query("SELECT e FROM Evenement e WHERE e.idEvenement = ?1")
    Optional<EvenementEntity> findEvenementEntitiesByIdEvenement(String idEvenement);

    @Transactional
    @Modifying
    @Query("DELETE FROM Evenement e WHERE e.idEvenement = ?1")
    int deleteEvenementByIdEvenement(Long idEvenement);

    Optional<EvenementEntity> findByIdEvenementAndUtilisateurEntity_IdUtilisateur(int idUtilisateur, int idUtilisateur1);

    List<EvenementEntity> findAllByUtilisateurEntity_IdUtilisateur(int idUtilisateur);

    List<EvenementEntity> findByUtilisateurEntity_IdUtilisateur(int idUtilisateur);

    List<EvenementEntity> findByLieuEntity_IdLieu(int idLieu);
}
