package com.mgen.amie.repository;

import com.mgen.amie.composite.IdCorrespondre;
import com.mgen.amie.entity.CorrespondreEntity;
import com.mgen.amie.entity.EvenementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface CorrespondreRepository extends JpaRepository<CorrespondreEntity, Integer> {
    public boolean existsById(IdCorrespondre id);

    List<CorrespondreEntity> findByEvenementEntity_IdEvenement(int idEvenement);

    List<CorrespondreEntity> findAllById_IdEvenement(int idEvenement);

    List<CorrespondreEntity> findByEvenementEntity(EvenementEntity evenementEntity);

    Optional<CorrespondreEntity> findById(IdCorrespondre id);

    boolean existsByIdIdTypologieEvenements(int idTypologieEvenements);

    @Transactional
    public void deleteByEvenementEntity_IdEvenement(int idEvenement);

    List<CorrespondreEntity> findById(int idEvenement);

    List<CorrespondreEntity> findAllByEvenementEntity(EvenementEntity evenement);
}