package com.mgen.amie.repository;

import com.mgen.amie.composite.IdInscriptions;
import com.mgen.amie.entity.EvenementEntity;
import com.mgen.amie.entity.InscriptionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface InscriptionsRepository extends JpaRepository<InscriptionsEntity, Integer> {
    boolean existsById(IdInscriptions id);

    Optional<Object> findById(IdInscriptions idInscriptions);

    void deleteById(IdInscriptions idInscription);

    List<InscriptionsEntity> findAllByUtilisateurEntity_IdUtilisateur(int idUtilisateur);

    List<InscriptionsEntity> findByEvenementEntity(EvenementEntity evenement);
}
