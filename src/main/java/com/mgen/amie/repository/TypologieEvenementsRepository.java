package com.mgen.amie.repository;

import com.mgen.amie.entity.TypologieEvenementsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface TypologieEvenementsRepository extends JpaRepository<TypologieEvenementsEntity, Integer> {


    public boolean existsByLabel(String label);

    public boolean existsById(int idTypologieEvenements);

    public TypologieEvenementsEntity findByIdTypologieEvenements(int idTypologieEvenements);

    void deleteByIdTypologieEvenements(int idEvenement);
}
