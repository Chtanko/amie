package com.mgen.amie.dto.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageContactGlobalDto {

    private int idUtilisateur;
    private String objet;
    private String description;

}
