package com.mgen.amie.dto.message;

import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import com.mgen.amie.entity.MessageContactEntity;
import com.mgen.amie.entity.UtilisateurEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GestionMessageContactDto {

    private int idMessageContact;
    private List<UtilisateurDto> utilisateur;
    private String objet;
    private String description;

    public void setMessages(List<MessageContactDto> customMessages) {
    }

    public void setUtilisateurDto(UtilisateurDto utilisateurDto) {
    }
}
