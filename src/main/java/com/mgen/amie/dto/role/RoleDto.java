package com.mgen.amie.dto.role;

public class RoleDto {

    private String libelle;

    public RoleDto() {
    }

    public RoleDto(int idRole, String libelle) {

        this.libelle = libelle;
    }


    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
