package com.mgen.amie.dto.faq;

public class RedactionFAQDto {

    private String question;
    private String description;
    public RedactionFAQDto() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
