package com.mgen.amie.dto.utilisateur;

import com.mgen.amie.dto.attribuer.AttribuerDto;
import com.mgen.amie.dto.role.RoleDto;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UtilisateurAvecRoleDto {
    private int idUtilisateur;
    private String prenom;
    private String nom;
    private String mail;
    private LocalDateTime dateDerniereConnexion;
    private LocalDateTime dateDerniereInitialisationMotDePasse;
    private List<RoleDto> roles;
    private List<AttribuerDto> attributions;

}
