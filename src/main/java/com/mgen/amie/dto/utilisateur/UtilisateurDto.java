package com.mgen.amie.dto.utilisateur;


import com.mgen.amie.dto.role.RoleDto;
import lombok.*;

import java.util.List;
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UtilisateurDto {
    private int idUtilisateur;
    private String nom;
    private String prenom;
    private String mail;
    private List<RoleDto> roles;
}
