package com.mgen.amie.dto.utilisateur;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UtilisateurAjoutDto {

    private String nom;
    private String prenom;
    private String mail;
    private String motDePasse;

}
