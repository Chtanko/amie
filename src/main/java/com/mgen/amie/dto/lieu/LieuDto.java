package com.mgen.amie.dto.lieu;

import lombok.*;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LieuDto {

    private int idLieu;
    private String coordoneesGps;
    private String adresse;
    private String ville;
    private String codePostal;
    private String localisation;
    private int places;

}
