package com.mgen.amie.dto.ajouterfavoris;

import com.mgen.amie.composite.IdInscriptions;
import com.mgen.amie.dto.lieu.LieuDto;
import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.dto.evenement.EvenementCompletDto;
import com.mgen.amie.dto.typologieevenements.TypologieEvenementsDto;
import com.mgen.amie.dto.utilisateur.UtilisateurAvecRoleDto;
import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AjouterFavorisDto {

    private int idUtilisateur;
    private int idEvenement;
    private String label;
    private String description;
    private String date_debut;
    private String date_fin;
    private LocalDateTime dateCreationEvenement;
    private String heure_debut;
    private String heure_fin;
    private String lien_replay;
    private String lien_ressources;
    private byte[] image;
    private LocalDateTime dateValidation;
    private String statut;
    private LieuDto lieu;
    private UtilisateurDto utilisateur;
    private List<TypologieEvenementsDto> typologieEvenements;

    public void setEvenement(EvenementCompletDto evenementDto) {
    }

    public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }

    public int getIdUtilisateur() {return idUtilisateur;}

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public void setEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }

    public void setIdInscription(IdInscriptions id) {
    }
}
