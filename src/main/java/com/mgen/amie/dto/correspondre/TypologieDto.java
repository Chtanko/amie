package com.mgen.amie.dto.correspondre;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TypologieDto {
    private Integer idTypologieEvenements;
}
