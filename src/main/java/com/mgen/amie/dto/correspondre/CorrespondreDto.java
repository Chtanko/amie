package com.mgen.amie.dto.correspondre;

import com.mgen.amie.composite.IdCorrespondre;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CorrespondreDto {

    private Integer idEvenement;

    private List<TypologieDto> idTypologieEvenements;
}
