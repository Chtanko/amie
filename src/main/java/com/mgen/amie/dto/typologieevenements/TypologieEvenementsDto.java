package com.mgen.amie.dto.typologieevenements;

import lombok.*;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TypologieEvenementsDto {

    private int idTypologieEvenements;
    private String label;

}
