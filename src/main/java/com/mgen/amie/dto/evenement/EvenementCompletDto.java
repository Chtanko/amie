package com.mgen.amie.dto.evenement;

import com.mgen.amie.dto.lieu.*;
import com.mgen.amie.dto.role.RoleDto;
import com.mgen.amie.dto.typologieevenements.TypologieEvenementsDto;
import com.mgen.amie.dto.utilisateur.UtilisateurDto;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EvenementCompletDto {

    private int idEvenement;
    private String label;
    private String description;
    private String date_debut;
    private String date_fin;
    private LocalDateTime dateCreationEvenement;
    private String heure_debut;
    private String heure_fin;
    private String lien_replay;
    private String lien_ressources;
    private byte[] image;
    private LocalDateTime dateValider;
    private String statut;
    private LieuDto lieu;
    private LocalDateTime dateValidation;
    private UtilisateurDto utilisateur;
    private List<TypologieEvenementsDto> typologieEvenements;

    public void setRoles(List<RoleDto> rolesDto) {
        this.utilisateur.setRoles(rolesDto);
    }
}



