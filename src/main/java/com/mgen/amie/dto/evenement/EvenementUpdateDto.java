package com.mgen.amie.dto.evenement;

import com.mgen.amie.dto.lieu.LieuDto;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
@Getter
@Setter
public class EvenementUpdateDto {

    private String date_debut;
    private String date_fin;
    private String description;
    private String heure_debut;
    private String heure_fin;
    private byte[] image;
    private String label;
    private String lien_replay;
    private String lien_ressources;
    private LocalDateTime dateValidation;
    private String statut;
    private List<Integer> idTypologieEvenements;
    private int idLieu;
}
