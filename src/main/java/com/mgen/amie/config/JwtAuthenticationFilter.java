package com.mgen.amie.config;

import com.mgen.amie.service.JwtService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    /* Ce filtre d'authentification JWT personnalisé est utilisé pour
     vérifier et valider les tokens JWT dans les requêtes HTTP.

    Si un token JWT valide est présent dans l'en-tête "Authorization" de la requête,
    l'utilisateur correspondant est authentifié et connecté automatiquement.
    */

    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String userEmail;
        if(authHeader == null || !authHeader.startsWith("Bearer ")){
            filterChain.doFilter(request, response);
            return;
        }
        jwt = authHeader.substring(7);

        //verifie si le token est valide
        userEmail = jwtService.extractUsername(jwt);
        if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {//verifie si l'utilisateur est connecté
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);//si l'utilisateur est connecté, on passe au filtre suivant
            if (jwtService.isTokenValid(jwt, userDetails)) {//verifie si l'utilisateur est valide
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                authToken.setDetails(
                    //on passe les détails de l'utilisateur au contexte de sécurité de spring security
                    new WebAuthenticationDetailsSource().buildDetails(request)
                );
                //on passe l'utilisateur au contexte de sécurité de spring security pour qu'il soit connecté automatiquement
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
    // verifie si l'utilisateur est connecté et valide et passe au filtre suivant
    filterChain.doFilter(request, response);
    }
}
