package com.mgen.amie.config;

import com.mgen.amie.entity.AttribuerEntity;
import com.mgen.amie.entity.UtilisateurEntity;
import com.mgen.amie.repository.AttribuerRepository;
import com.mgen.amie.repository.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {

    private final UtilisateurRepository utilisateurRepository;
    private final AttribuerRepository attribuerRepository;

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> {
            UtilisateurEntity user = utilisateurRepository.findByMail(username)
                    .orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvé"));

            List<AttribuerEntity> attributions = attribuerRepository.findByUtilisateurEntityIdUtilisateur(user.getIdUtilisateur());
            List<GrantedAuthority> authorities = attributions.stream()
                    .map(attribution -> new SimpleGrantedAuthority(attribution.getRole().getLibelle()))
                    .collect(Collectors.toList());

            return new User(user.getMail(), user.getMotDePasse(), authorities);
        };
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @EnableCaching
    public class CachingConfig {

        @Bean
        public CacheManager cacheManager() {
            return new ConcurrentMapCacheManager("evenementsCache");
        }

    }
}
