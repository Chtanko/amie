import { Link, Navigate, useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError();
  

  return (
    <div id="error-page" className="h-screen w-screen grid place-content-center place-items-center">
       
      <Navigate to={'/login'} replace={true} />
      
      <div>
        <h1 className="font-bold text-5xl">Oups!</h1>
        <p className="my-6">Désolé, une erreur est apparue :</p>
        <p>
          {/* @ts-ignore */}
          <i className="text-black/20 font-medium my-6">{error.statusText || error.message}</i>
        </p>
        <Link to={'/'} className="underline hover:text-primary">Retour à l'accueil</Link>
      </div>
    </div>
  );
}