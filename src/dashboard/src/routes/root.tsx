import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import Menu from '../components/Layout/Menu'
import { useState } from 'react';
import Login from './login';
import { AiOutlineLoading, AiOutlineLogout } from 'react-icons/ai';


const Root = () => {
  const [isLogged, setIsLogged] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  fetch('http://amie.labinno-mtech.fr/api/faq/getallfaq', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'Authorization': String(localStorage.getItem('token'))
      }
  }).then(r => {setIsLogged(!(r.status === 403)); setIsLoading(false)})
  const location = useLocation()
  const navigate = useNavigate()
  return (
    <div className=''>
      {
        location.pathname === "/" ?
        <div>
          <ul className="p-2 h-full min-h-screen gap-4 pt-6 font-semibold rounded-none fixed">
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <button onClick={() => {localStorage.setItem('token',''); navigate('/login'); window.location.reload()}} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineLogout size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Se déconnecter</p>
                </button>
              </li>
          </ul>
        </div>
        :
        <Menu log={isLogged}/>
      }
        <div>
          {
            isLogged ?
            <div className='-z-50'>
              <Outlet />
            </div>
            :
            isLoading ?
            <div className='w-screen h-screen grid place-content-center place-items-center'>
              <AiOutlineLoading className="animate-spin w-44 h-44 text-primary" />
            </div>
            :
            <Login />
          }
        </div>
    </div>
  )
}

export default Root