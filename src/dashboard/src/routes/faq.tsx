import { Faq } from "../../typing";
import { fetchAddFaq, fetchDeleteFaq, fetchFaq, fetchModifyFaq } from "../api";
import Title from "../components/Layout/Title";
import { AddFaqButton } from "../components/FAQ/Buttons/AddFaqButton";
import { ModifyFaqButton } from "../components/FAQ/Buttons/ModifyFaqButton";
import { useEffect, useState } from "react";

const Faqs = () => {
  const [db, setDb] = useState([])
  const [msg, setMsg] = useState("")
  const [clearMsg, setClearMsg] = useState("")
  const [isInvalid, setIsInvalid] = useState(false)
    // Chargement des données
    useEffect(() => {
        const getData = async () => {
            await fetchFaq().then(r => setDb(r))
        }
        getData()
         
    }, [clearMsg])

    const addFaq = async (body: any, id: number) => {
      await fetchAddFaq(id, body).then(r => {setIsInvalid(!(/^Ajout/.test(r))); setMsg(r)} )
      setTimeout(() => setMsg(""), 3000)
      clearMsg === "add" ? setClearMsg("") :setClearMsg("add")
  }

  const deleteFaq = async (faq: Faq) => {
      await fetchDeleteFaq(faq.idFaq).then((r: string) => {setIsInvalid(!(/^FAQ supprimée/.test(r))) ; setMsg(r)})
      setTimeout(() => setMsg(""), 3000)
      clearMsg === "delete" ? setClearMsg("") : setClearMsg("delete")
  }
  
  const modifyFaq = async (id: number, body: any) => {
      await fetchModifyFaq(id, body).then(r => {setIsInvalid(!(/^FAQ modifiée/.test(r))) ; setMsg(r)})
      setTimeout(() => setMsg(""), 3000)
      clearMsg === "modify" ? setClearMsg("") :setClearMsg("modify")
  }
    
  return (
    <div className='w-3/4 mx-auto my-6'>
            <Title>FAQ</Title>
            <AddFaqButton addFaq={addFaq}/>
            <div className="">
            <span className={`fixed rounded-lg p-4 w-fit animate-fadeOut top-4 left-4 ${msg === "" ? "hidden" : ""} ${isInvalid ? "bg-error text-white" : "bg-secondary text-primary"}`}>{msg}</span>
              <div className="mt-12 transition-all">
                  <table className="transition-all w-full rounded">
                      {/* <!-- head --> */}
                      <thead >
                      <tr className="h-12 text-neutral bg-neutral/5">
                          <th className='p-4 w-28 rounded-tl-lg'>Question</th>
                          <th className='p-4 w-28'>Réponse</th>
                          <th className='p-4 w-44'>Utilisateur</th>
                          <th className="p-4 w-12 rounded-tr-lg">Modifier</th>
                      </tr>
                      </thead>
                      <tbody className="transition-all duration-150">
                          {db.map((faq: Faq) =>( 
                                  <tr className="group h-12 transition-all hover:bg-neutral/5 border-b border-neutral/5" key={faq.idFaq}>
                                      <th className="p-4 font-light transition-all text-xs"> {faq.question}</th>
                                      <th className="p-4 font-light transition-all text-xs">{faq.description}</th>
                                      <th className="p-4 transition-all font-medium">{faq.utilisateur[0].prenom} {faq.utilisateur[0].nom}</th>
                                      <th className="p-4 h-12">
                                          <ModifyFaqButton faq={faq} deleteFaq={deleteFaq} modifyFaq={modifyFaq}/>
                                      </th>
                                  </tr>
                          ))}
                      </tbody>
                  </table>
              </div>
            </div>
      </div>
  )
}

export default Faqs