import Title from "../components/Layout/Title"
import { fetchDeleteMessages, fetchMessages,} from "../api"

import { Message, } from "../../typing"
import { useEffect, useState } from "react"

const Messages = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [db, setDb] = useState([])
    const [clearMsg, setClearMsg] = useState("clear")
    const [selectedMsg, setSelectedMsg]: any = useState()
    useEffect(() => {
        const getData = async () => await fetchMessages().then(r => {setDb(r); setIsLoading(false)})
        getData()
    }, [clearMsg])

    const onClick = (message: Message) => {
       setSelectedMsg(message)
    }

    const deleteItem = (message: Message) => {
        console.log(message)
        const i = db.indexOf(message as never)
        console.log(i)
        db.splice(i, 1)
        console.log(db)
        fetchDeleteMessages(String(message.idMessageContact))
        setClearMsg("delete")
        setSelectedMsg()
    }

    return (
        <div className='w-3/4 mx-auto my-6'>
            <Title>Messages</Title>
            <div className="h-full w-full border border-secondary shadow-mgen my-6 flex rounded-lg">
                <ul className="grid bg-secondary md:w-60 rounded-l-lg">
                    {
                        isLoading ?
                        <li className="flex items-center justify-center"><span className="loading loading-dots text-primary border loading-lg"></span></li>
                        :
                        db.map((message: Message) =>
                            <li key={message.idMessageContact} onClick={() => onClick(message)} className="flex justify-between p-2 text-primary hover:text-white hover:bg-primary transition-all cursor-pointer first:rounded-tl-lg last:rounded-bl-lg">
                                {message.objet}
                            </li>
                        )
                    }
                </ul>
                <MessageData message={selectedMsg} deleteItem={deleteItem}/>
            </div>
        </div>
    )
}

export default Messages

const MessageData = ({message, deleteItem} : {message?: Message, deleteItem: any}) => {
    if (!message) {
        return (
            <div className=" relative w-full">
                <div className="grid p-4 gap-3">
                    <h3 className={`text-xl font-bold border-b`}>Aucun message à afficher</h3>
                </div>
            </div>
        )
    }
    const user = message.utilisateur[0].nom + " " + message.utilisateur[0].prenom
    return (
        <div className=" relative w-full">
            <div className="grid p-4 gap-3">
                <h3 className={`text-xl font-bold ${message.objet === "" ? "" : "border-b"}`}>{message.objet}</h3>
                <h5 className={`${user === " " ? "hidden" : "block"} italic`}>De : {user}</h5>
                <p className="text-sm font-light mt-3">{message.description}</p>
                <div className="flex gap-2">
                    <a href={`mailto:${message.utilisateur[0].mail}?subject=Re: ${message.objet}`} className={`${message.utilisateur[0].mail === "" ? 'hidden' : 'block'} p-3 text-primary bg-secondary  hover:bg-primary rounded-lg w-24 hover:text-white`}>Répondre</a>
                    <form onSubmit={(e) => {e.preventDefault() ; deleteItem(message)}}>
                        <button type="submit" className={`${String(message.idMessageContact) === "" ? 'hidden' : 'block'} p-3 text-error bg-error/10  hover:bg-error rounded-lg w-24 hover:text-white`}>Supprimer</button>
                    </form>
                </div>
            </div>
        </div>
    )
}