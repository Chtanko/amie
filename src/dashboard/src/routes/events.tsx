import Title from "../components/Layout/Title";
import EventCard from "../components/Events/EventCard";
import { AddEventButton } from "../components/Events/Buttons/AddEventButton";
import { useQuery } from '@tanstack/react-query'
import { fetchAddEvent, fetchDeleteEvent, fetchEvent, fetchModifyEvent, fetchTypo } from "../api";
import { Evenment, TypoEvent } from "../../typing";
import { useEffect, useState } from "react";



export default function Events() {
  const tags = useQuery({ queryKey: ["tags"], queryFn: fetchTypo });
  const [selectedTag, setSelectedTag] = useState('');
  const [selectedDate, setSelectedDate] = useState('');
  const [selectedStatut, setSelectedStatut] = useState('');
  const [db, setDb]: any = useState([])
  const [msg, setMsg] = useState("")
  const [clearMsg, setClearMsg] = useState("")
  const [isInvalid, setIsInvalid] = useState(false)
  const [filteredEvents, setFilteredEvents] = useState(db);

  const applyFilters = () => {
    const filteredArray = db.filter((event: Evenment) => {
      
      const tagMatch = selectedTag === '' || event.typologieEvenements.some((tag) => tag.label === selectedTag);

      const currentDate = new Date();
      const startDate = new Date(event.date_debut);
      const endDate = new Date(event.date_fin);

      let dateMatch = true;

      switch (selectedDate) {
        case 'ongoing':
          dateMatch = startDate <= currentDate && currentDate <= endDate;
          break;
        case 'upcoming':
          dateMatch = startDate > currentDate;
          break;
        case 'past':
          dateMatch = endDate < currentDate;
          break;
        default:
          dateMatch = true;
          break;
      }
      const statutMatch = selectedStatut === '' || event.statut === selectedStatut;

      return tagMatch && dateMatch && statutMatch;
    });
    
    setFilteredEvents(filteredArray);
  }

  
  
  useEffect(() => {
    const getData = async () => {
        try {
            const response = await fetchEvent();
            setDb(response);
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };

    getData();
}, [selectedTag, selectedDate, selectedStatut, clearMsg]);

useEffect(() => {
    applyFilters();
}, [selectedTag, selectedDate, selectedStatut, clearMsg, db]);

  // Actions
  const addEvent = async (creator: string, lieu: string, body: any) => {
    await fetchAddEvent(creator, lieu, body).then(r => {setIsInvalid(/^Problème/.test(r)) ; setMsg(r)})
    setTimeout(() => setMsg(""), 3000)
  }

  const deleteEvent = async (id: number) =>  {
    await fetchDeleteEvent(id).then((r: string) => {setIsInvalid(/^L'événement n'existe pas/.test(r));setMsg(r)}).then(() => clearMsg === "delete" ? setClearMsg("") :setClearMsg("delete"))
    setTimeout(() => setMsg(""), 3000)
  }

  const modifyEvent = async (userId: number, eventId: number, body: any ) => {
    await fetchModifyEvent(userId, eventId, body).then(r => {setIsInvalid(/^Une erreur/.test(r)); setMsg(r); console.log(msg)})
    setTimeout(() => setMsg(""), 3000)
    clearMsg === "modify" ? setClearMsg("") :setClearMsg("modify")
  }

  return (
    <div className='w-3/4 mx-auto my-6'>
      <Title>Évènements</Title>
      <div>
      <span className={`fixed rounded-lg p-4 w-fit animate-fadeOut z-50 shadow top-4 left-4 ${msg === "" ? "hidden" : ""} ${isInvalid ? "bg-error text-white" : "bg-secondary text-primary"}`}>{msg}</span>
        <AddEventButton addEvent={addEvent}/>
        <div className="flex w-full  flex-wrap mt-4  gap-3">
          <select name="tag" id="tag" value={selectedTag} className="select select-primary w-52 whitespace-nowrap text-ellipsis" onChange={(e) => setSelectedTag(e.currentTarget.value)}>
            <option value="">Toutes les Catégories</option>
            {tags?.data?.map((tag: TypoEvent) => (
              <option value={tag.label} key={tag.idTypologieEvenements}>{tag.label}</option>
            ))}
          </select>
          <select name="date" id="date" value={selectedDate} className="select select-primary w-52 whitespace-nowrap text-ellipsis" onChange={(e) => setSelectedDate(e.currentTarget.value)}>
            <option value="">Toutes les dates</option>
            <option value="ongoing">En cours</option>
            <option value="upcoming">À venir</option>
            <option value="past">Passé</option>
          </select>
          <select name="statut" id="status" value={selectedStatut} className="select select-primary w-52 whitespace-nowrap text-ellipsis" onChange={(e) => setSelectedStatut(e.currentTarget.value)}>
            <option value="">Tous les statuts</option>
            <option value="En Attente">En attente</option>
            <option value="Accepté">Accepté</option>
            <option value="Refusé">Refusé</option>
          </select>
        </div>
      </div>
      {
        db.length === 0 ? 
          <div className="w-full h-[65vh] grid place-items-center place-content-center">
            <span className="loading loading-spinner text-primary w-40"></span>
          </div>
        :
          filteredEvents ? (
            <div className="my-4 grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4">
              {filteredEvents.map((event: Evenment) => (
                <EventCard key={event.idEvenement} event={event} deleteEvent={deleteEvent} modifyEvent={modifyEvent}/>
                ))}
            </div>
          )
          : 
            <div className="w-full h-[65vh] grid place-items-center place-content-center">
              <span className="loading loading-spinner loading-lg text-primary text-5xl"></span>
            </div>
      }
    </div>
  );
}
