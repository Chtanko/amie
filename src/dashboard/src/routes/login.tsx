import { useLocation, useNavigate } from "react-router-dom";
import { fetchLogin } from "../api";
import Title from "../components/Layout/Title";

const Login = () => {
    // il faut enregistrer la "location" du user
    const navigate = useNavigate()
    const {state} = useLocation()
    const onConnect = async (e: any) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        
        const body = {
            "mail" : entries.mail,
            "motDePasse" : entries.motDePasse
        }
        const res = await fetchLogin(body); 
        
        localStorage.setItem('token', res.token)
        
        navigate(state?.path || "/")
        window.location.reload();
    };

  return (
    <div className='w-3/4 h-full mx-auto my-6'>
        <Title>Connexion</Title>
        <div className="w-full h-[75vh] grid place-items-center place-content-center">
            <div className="border p-4 border-secondary shadow-mgen rounded-lg">
                <form onSubmit={onConnect} className="grid gap-4">
                    <input type="email" name="mail" className="input input-bordered" placeholder="Email"/>
                    <input type="password" name="motDePasse" className="input input-bordered" placeholder="Mot de passe"/>
                    <button className="btn btn-primary text-white">Se connecter</button>
                </form>
            </div>
        </div>
    </div>
  )
}

export default Login