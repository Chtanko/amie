import Title from "../components/Layout/Title";
// import { Board } from "../components/Home/Board";
// import { Card } from "../components/Home/Card";
// import { Evenment, Lieu } from "../../typing";
// import { useQuery } from "@tanstack/react-query";
// import { fetchEvent, fetchLieux } from "../api";
import { AiOutlineCalendar, AiOutlineEnvironment, AiOutlineMail, AiOutlineQuestionCircle, AiOutlineTag, AiOutlineUser } from "react-icons/ai";

export default function Home() {
    // const todayDate = new Date().toISOString();
    // const lieux = useQuery({
    //     queryKey: ['lieux'],
    //     queryFn: fetchLieux
    // });
    // const events = useQuery({
    //     queryKey: ["events"],
    //     queryFn: fetchEvent
    // })

    return (
        <div className='w-3/4 mx-auto h-screen'>
            <Title>Accueil</Title>
            <div className="mt-12 w-3/4 mx-auto grid place-content-center place-items-center gap-16 grid-cols-2 lg:grid-cols-3">
                
                <a href="/events" className="w-full group">
                    <AiOutlineCalendar className='home-icon'/>
                    <h3 className="home-title">Evenements</h3>
                </a>
                <a href="/users" className="w-full group">
                    <AiOutlineUser className='home-icon'/>
                    <h3 className="home-title">Utilisateurs</h3>    
                </a>
                <a href="/lieux" className="w-full group">
                    <AiOutlineEnvironment className='home-icon'/>
                    <h3 className="home-title">Lieux</h3>
                </a>
                <a href="/faq" className="w-full group">
                    <AiOutlineQuestionCircle className='home-icon'/>
                    <h3 className="home-title">FAQ</h3>
                </a>
                <a href="/tag" className="w-full group">
                    <AiOutlineTag className='home-icon'/>
                    <h3 className="home-title">Typologie</h3>
                </a>
                <a href="/messages" className="w-full group">
                    <AiOutlineMail className='home-icon'/>
                    <h3 className="home-title">Messages</h3>   
                </a>
            </div>
        </div>
    )
}