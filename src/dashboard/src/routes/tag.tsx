import Title from '../components/Layout/Title'
import { fetchAddTag, fetchDeleteTag, fetchTypo } from '../api'
import { TypoEvent } from '../../typing'
import { AiOutlineCloseCircle, AiOutlinePlus } from 'react-icons/ai'
import { useEffect, useState } from 'react'

const Tags = () => {
    const [msg, setMsg] = useState("")
    const [db, setDb] = useState([])
    const [clearMsg, setClearMsg] = useState("clear")
    const [valid, setValid] = useState(true)
    useEffect(() => {
        const getData = async () => await fetchTypo().then(r => {setDb(r)})
        getData()
    }, [clearMsg])

    const isTypologieMessage = (message: string) => {
        const regex = /^Typologie/;
        return regex.test(message);
    }

    const addTag = async (event: any) => {
        event.preventDefault();
        const formData = new FormData(event.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        const body = {
            "label": entries.label,
        }
        // @ts-ignore
        document.getElementById('tag').value = ""
        await fetchAddTag(body).then(r => {setValid(isTypologieMessage(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "add" ? setClearMsg("") :setClearMsg("add")
    }

    const deleteTag = async (id: number) => {
        await fetchDeleteTag(id).then(r => { setValid(isTypologieMessage(r)) ; setMsg(r)});
        setTimeout(() => setMsg(""), 3000);
        clearMsg === "delete" ? setClearMsg("") : setClearMsg("delete")
    }

  return (
    <div className='w-3/4 mx-auto my-6'>
    <Title>Tags</Title>
    <form className='my-6 flex h-12 items-center' onSubmit={addTag}>
        <input name='label' type="text" id='tag' className='p-2 border border-secondary rounded-l-lg outline-none text-lg'/>
        <button type='submit' className='p-3 text-2xl flex justify-center text-primary bg-secondary items-center hover:text-base-100 hover:bg-primary transition-all rounded-r-lg flex-col'><AiOutlinePlus className='w-6 h-6'/></button>
        <span className={`fixed rounded-lg p-4 w-fit animate-fadeOut top-4 left-4 ${valid ? "bg-secondary text-primary" : "bg-error text-white"} ${msg === "" ? 'hidden' : ""}`}>{msg}</span>
    </form>
    <div className='w-full flex flex-wrap gap-6'>
        {
            db.map((tag: TypoEvent) => 
                <div className="badge badge-secondary text-primary pr-0" key={tag.idTypologieEvenements}>
                    <span>{tag.label}</span>
                    <button onClick={() => deleteTag(tag.idTypologieEvenements)} className='ml-2 rounded-full hover:bg-primary hover:text-secondary border-spacing-0'><AiOutlineCloseCircle className="w-4 h-4 mr-0" /></button>
                </div>
            )
        }
    </div>
    </div>
  )
}

export default Tags