
import Title from "../components/Layout/Title";
import { AddLieuButton } from "../components/Lieux/Buttons/AddLieuButton";
import { ModifyLieuButton } from "../components/Lieux/Buttons/ModifyLieuButton";
import { fetchAddLieu, fetchDeleteLieu, fetchLieux, fetchModifyLieu } from "../api";
import { useEffect, useState } from "react";
import { Lieu } from "../../typing";


export default function Lieux() {
  const [db, setDb]: any = useState([])
  const [msg, setMsg] = useState("")
  const [clearMsg, setClearMsg] = useState("clear")
  const [isValid, setIsValid] = useState(false)
    // Chargement des données
    useEffect(() => {
        const getData = async () => {
            await fetchLieux().then(r => setDb(r))
        }
        getData()
         
    }, [clearMsg])

    // Actions
    const addLieu = async (body: any) => {
      await fetchAddLieu(body).then(r => {setIsValid(/^Lieu ajouté/.test(r)); setMsg(r)} )
      setTimeout(() => setMsg(""), 3000)
      clearMsg === "add" ? setClearMsg("") :setClearMsg("add")
    }

    const deleteLieu = async (lieu: Lieu) => {
        await fetchDeleteLieu(lieu.idLieu).then((r: string) => {setIsValid(/^Lieu supprimé/.test(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "delete" ? setClearMsg("") : setClearMsg("delete")
    }
    
    const modifyLieu = async (id: number, body: any) => {
        await fetchModifyLieu(id, body).then(r => {setIsValid(/^Le lieu est mis à jour/.test(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "modify" ? setClearMsg("") :setClearMsg("modify")
    }

    return (
      <div className='w-3/4 mx-auto my-6'>
            <Title>Lieux</Title>
            <AddLieuButton  addLieu={addLieu}/>
            <div className="">
            <span className={`fixed rounded-lg p-4 w-fit animate-fadeOut top-4 left-4 ${msg === "" ? "hidden" : ""} ${isValid ? "bg-secondary text-primary": "bg-error text-white"}`}>{msg}</span>
              <div className="mt-12 transition-all">
                  <table className="transition-all w-full rounded">
                      {/* <!-- head --> */}
                      <thead >
                      <tr className="h-12 text-neutral bg-neutral/5">
                          <th className='p-4 w-28 rounded-tl-lg'>Adresse</th>
                          <th className='p-4 w-28'>Code Postal</th>
                          <th className='p-4 w-44'>Ville</th>
                          <th className='p-4 w-40'>Localisation</th>
                          <th className="p-4 w-12 rounded-tr-lg">Modifier</th>
                      </tr>
                      </thead>
                      <tbody className="transition-all duration-150">
                          {db.map((lieu: Lieu) =>( 
                                  <tr className="group h-12 transition-all hover:bg-neutral/5 border-b border-neutral/5" key={lieu.idLieu}>
                                      <th className="p-4 font-light capitalize transition-all text-xs">{lieu.adresse}</th>
                                      <th className="p-4 font-light capitalize transition-all"> {lieu.codePostal}</th>
                                      <th className="p-4 transition-all">{lieu.ville}</th>
                                      <th className="p-4 font-light transition-all text-sm">{lieu.localisation}</th>
                                      <th className="p-4 h-12">
                                          <ModifyLieuButton lieu={lieu} modifyLieu={modifyLieu} deleteLieu={deleteLieu} />
                                      </th>
                                  </tr>
                          ))}
                      </tbody>
                  </table>
              </div>
            </div>
      </div>
    )
}
  