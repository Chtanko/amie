import { ModifyUserButton } from "../components/Users/Buttons/ModifyUserButton";
import Title from "../components/Layout/Title";
import { AddUserButton } from "../components/Users/Buttons/AddUSerButton";
import { fetchAddRole, fetchAddUser, fetchDeleteUser, fetchModifyUser, fetchUsers } from "../api";
import { AddRoleButton } from "../components/Users/Buttons/AddRoleButton";
import { useEffect, useState } from "react";
import { User } from "../../typing";

export default function Users() {
    const [db, setDb]: any = useState([])
    const [msg, setMsg] = useState("")
    const [clearMsg, setClearMsg] = useState("")
    const [isInvalid, setIsInvalid] = useState(false)
    // Chargement des données
    useEffect(() => {
        const getData = async () => {
            await fetchUsers().then(r => setDb(r))
        }
        getData()
         
    }, [clearMsg])

    const isValidMessage = (message: string) => {
        const regex = /^Erreur/;
        return regex.test(message);
    }
    // Actions
    const addUser = async (body: any) => {
        await fetchAddUser(body).then(r => {if (r.ok) {setIsInvalid(false) ; setMsg("Utilisateur ajouté")} else {setIsInvalid(true) ; setMsg("Erreur lors de l'ajout")}} )
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "add" ? setClearMsg("") :setClearMsg("add")
    }

    const deleteUser = async (user: User) => {
        await fetchDeleteUser(user.idUtilisateur).then(r => {setIsInvalid(/^Impossible/.test(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "delete" ? setClearMsg("") :setClearMsg("delete")
    }
    
    const modifyUser = async (user: User) => {
        await fetchModifyUser(user.idUtilisateur, {
                "mail": user.mail,
                "motDePasse": "",
                "nom": user.nom,
                "prenom": user.prenom
        }).then(r => {setIsInvalid(isValidMessage(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "modify" ? setClearMsg("") :setClearMsg("modify")
    }

    const addRole = async (id:string, idRole: string) => {
        await fetchAddRole(id, idRole).then(r => {setIsInvalid(isValidMessage(r)) ; setMsg(r)})
        setTimeout(() => setMsg(""), 3000)
        clearMsg === "addRole" ? setClearMsg("") :setClearMsg("addRole")
    }


   return (
        <div className='w-3/4 mx-auto my-6'>
             <Title>Utilisateurs</Title>
             <AddUserButton addUser={addUser}/>
             <div className="">
                <span className={`fixed rounded-lg p-4 w-fit animate-fadeOut top-4 left-4 ${msg === "" ? "hidden" : ""} ${isInvalid ? "bg-error text-white" : "bg-secondary text-primary"}`}>{msg}</span>
                <div className="mt-12 transition-all">
                    <table className="transition-all w-full">
                        {/* <!-- head --> */}
                        <thead >
                        <tr className="h-12 text-neutral bg-neutral/5">
                            <th className='p-4 w-28'>Nom</th>
                            <th className='p-4 w-28'>Prénom</th>
                            <th className='p-4 w-44'>Email</th>
                            <th className='p-4 w-40'>Rôle</th>
                            <th className="p-4 w-12">Modifier</th>
                        </tr>
                        </thead>
                        <tbody className="transition-all duration-150">
                            {db.map((user: User) =>( 
                                    <tr className="group transition-all hover:bg-neutral/5 border-b border-secondary" key={user.idUtilisateur}>
                                        <th className="p-4 h-fit font-light uppercase transition-all">{user.nom}</th>
                                        <th className="p-4 h-fit font-light capitalize transition-all"> {user.prenom}</th>
                                        <th className="p-4 h-fit font-light transition-all">{user.mail}</th>
                                        <th key={user.idUtilisateur} className="p-4 font-light transition-all flex justify-between items-center group">
                                            <div>

                                                {
                                                    user.roles.map(role => 
                                                        <p key={role.idRole}>{role.libelle}</p>
                                                        )
                                                }
                                            </div>
                                            <AddRoleButton addRole={addRole} id={user.idUtilisateur}
                                             />
                                        </th>
                                        <th className="p-4 ">
                                            <ModifyUserButton user={user} deleteUser={deleteUser} modifyUser={modifyUser}/>
                                        </th>
                                    </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
             </div>
        </div>
    )
}
  