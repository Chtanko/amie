import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Root from "./routes/root"
import Home from './routes/home'
import ErrorPage from './error-page'
import Events from './routes/events'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import Users from './routes/users'
import Lieux from './routes/lieux'
import Faq from './routes/faq'
import Tags from './routes/tag'
import Login from './routes/login'
import Messages from './routes/messages'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      { 
        path: "/",
        element: <Home />
      },
      { 
        path: "/events",
        element: <Events />,
      },
      { 
        path: "/users",
        element: <Users />
      },
      {
        path: "/lieux",
        element: <Lieux />,
      },
      {
        path: "/faq",
        element: <Faq />,
      },
      {
        path: "/tag",
        element: <Tags />,
      },
      {
        path: "/messages",
        element: <Messages />,
      },
      {
        path: "/login",
        element: <Login />,
      },
    ]
  }
])

const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  </React.StrictMode>,
)
