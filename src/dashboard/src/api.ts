import { Evenment, User } from "../typing";

const accesToken = String(localStorage.getItem("token"))


// - EVENTS
export const fetchEvent = async () => {
    const res = await fetch("http://amie.labinno-mtech.fr/api/evenement/getallevenements", {
      method: 'GET',
      headers: {
          'Authorization' : accesToken ,
          'Content-Type': 'application/json'
      }
    })
    const events: Evenment[] = await res.json();
    return events;
}

export const fetchAddEvent = async (creator: string, lieu:string, body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/evenement/createevenement/${creator}/${lieu}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : accesToken
            },
            body: JSON.stringify(body)
        }
    ).then(r => r.text());
    return res
}

export const fetchModifyEvent = async (user: number, event: number, body: any ) => {
    const res =await fetch(`http://amie.labinno-mtech.fr/api/evenement/${user}/${event}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : accesToken
        },
        method: 'PUT',
        body: JSON.stringify(body)
        }
    ).then(r => r.text());
    return res
}

export const fetchDeleteEvent = async (id: number) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/evenement/deleteevenement/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': accesToken
      }
    }).then(r => r.text())
    return res
}

// Inscription EVENT
export const fetchInscriptionEvent = async () => {
    const res = await fetch('http://amie.labinno-mtech.fr/api/evenement/getallinscriptions', {
        method: 'GET',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
          }
        }).then(r => r.json())
    return res;
}


// - USERS
export const fetchUsers = async () => {
    const res = await fetch('http://amie.labinno-mtech.fr/api/utilisateurs/getallutilisateursavecroles', {
        method: 'GET',
        headers: {
            'Authorization' : accesToken ,
            'Content-Type': 'application/json'
        }
    })
    const users: User[] = await res.json()
    return users;
}

export const fetchAddUser = async (body: any) => {
    const res = await fetch("http://amie.labinno-mtech.fr/api/utilisateurs/addutilisateur", {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': accesToken
            },
            method: 'POST',
            body: JSON.stringify(body)
        }
    )
    return res
}

export const fetchModifyUser = async (id: number, body:any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/utilisateurs/updateutilisateur/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': accesToken
        },
      method:'PUT',
      body: JSON.stringify(body),
    }).then(r => r.text());
    return res
}

export const fetchDeleteUser = async (id:number) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/utilisateurs/deleteutilisateur/${id}`,{
        method:'DELETE',
        headers:{
            'Authorization': accesToken
        }
    }).then((r) => r.text())
    return res
}


// - LIEUX
export const fetchLieux = async () => {
    const res = await fetch('http://amie.labinno-mtech.fr/api/lieu/getalllieux', {
        method: 'GET',
        headers: {
          'Authorization': accesToken,
          'Content-Type': 'application/json'
        }
      }).then(r => r.json())
    return res;
}

export const fetchAddLieu = async (body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/lieu/addlieu`,{
     headers: {
        'Content-Type': 'application/json',
        'Authorization' : accesToken
        },
      method:'POST',
      body: JSON.stringify(body),
    }).then(r => r.text());
    return res
}

export const fetchModifyLieu = async (id: number, body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/lieu/updatelieu/${id}`,{
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : accesToken
        },
        method:'PUT',
        body: JSON.stringify(body),
    }).then(r => r.text());
    return res
}

export const fetchDeleteLieu = async (id:number) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/lieu/deletelieu/${id}`,{
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : accesToken
            },
      method:'DELETE',
    }).then(r => r.text());
    return res
}

// - F.A.Q

export const fetchFaq = async () => {
    const res = await fetch('http://amie.labinno-mtech.fr/api/faq/getallfaq', {
        method: 'GET',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
          }
        }).then(r => r.json())
    return res 
}

export const fetchAddFaq = async (userId: number, body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/faq/addfaq/${userId}`,{
     headers: {
        'Content-Type': 'application/json',
        'Authorization' : accesToken
        },
      method:'POST',
      body: JSON.stringify(body),
    }).then(r => r.status === 200 ? r.text() : "Une erreur s'est produite.")
    return res
}

export const fetchModifyFaq = async (id: number, body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/faq/updatefaq/${id}`, {
        method: 'PUT',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(r => r.text())
    return res
}

export const fetchDeleteFaq = async (id: number) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/faq/removefaq/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        }
    }).then(r => r.text())
    return res

}

// - Typologie d'évènements

export const fetchTypo = async () => {
    const res = await fetch('http://amie.labinno-mtech.fr/api/typologieevenement/getalltypologieevenements', {
        method: 'GET',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
          }
        }).then(r => r.json())
    return res
}

export const fetchAddTag = async (body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/typologieevenement/addtypologieevenement?label=${body.label}`, {
        method: 'POST',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        },
    }).then(r => r.text())
    return res
}

export const fetchDeleteTag = async (id: number) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/typologieevenement/deletetypologieevenement/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        },
    }).then(r => r.text())
    return res
}

// Messages

export const fetchMessages = async () => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/messagecontactcontroller/getallmessages`, {
        method: 'GET',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        }
    }).then(r => r.json())
    return res
}

export const fetchDeleteMessages = async (id: string) => {
    await fetch(`http://amie.labinno-mtech.fr/api/messagecontactcontroller/deletemessagecontact/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        },
    })
}

// Roles
export const fetchRoles = async () => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/role/getallroles`, {
        method: 'GET',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        }
    }).then(r => r.json())
    return res
}

export const fetchAddRole = async (idUser: string, idRole: string) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/utilisateurs/${idUser}/${idRole}`, {
        method: 'POST',
        headers: {
            'Authorization': accesToken,
            'Content-Type': 'application/json'
        },
    }).then(r => r.text())
    return res
}

// Authenticate

export const fetchInscription = async () => {

}

export const fetchLogin = async (body: any) => {
    const res = await fetch(`http://amie.labinno-mtech.fr/api/authentification/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(r => r.json())
    return res
}

