import { AiOutlineHome, AiOutlineCalendar, AiOutlineUser, AiOutlineQuestionCircle, AiOutlineTag, AiOutlineLogout, AiOutlineLogin, AiOutlineMail, AiOutlineEnvironment} from 'react-icons/ai';

import { useNavigate } from 'react-router-dom';

function Menu({log} : {log: boolean}) {
  const navigate = useNavigate();
  return (
    <div className='z-50'>
      {
        log ?
            <ul className="p-2 h-full min-h-screen gap-4 pt-6 font-semibold rounded-none fixed z-50">
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/'} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineHome size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Accueil</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/events'} className="m-auto md:m-0 grid grid-cols-[3.5rem_1fr] items-center group">
                  <AiOutlineCalendar size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Évènements</p> 
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/users'} className="m-auto md:m-0 grid grid-cols-[3.5rem_1fr] items-center group ">
                  <AiOutlineUser size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Utilisateurs</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/lieux'} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineEnvironment size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Lieux</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/faq'} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineQuestionCircle size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>F.A.Q</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/tag'} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineTag size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Tags</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <a href={'/messages'} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineMail size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Messages</p>
                </a>
              </li>
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <button onClick={() => {localStorage.setItem('token',''); navigate('/login'); window.location.reload()}} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineLogout size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Se déconnecter</p>
                </button>
              </li>
            </ul>
              :
              // log out
            <ul className="p-2 h-full min-h-screen gap-4 pt-6 font-semibold rounded-none fixed">
              <li className='rounded-lg my-2 px-2 hover:bg-white/80 hover:backdrop-blur'>
                <button onClick={() => navigate('/login')} className="m-auto md:m-0 group grid grid-cols-[3.5rem_1fr] items-center">
                  <AiOutlineLogin size={40} className='p-2 bg-primary/10 text-primary rounded-lg group-hover:bg-primary group-hover:text-base-100 transition-all'/>
                  <p className='animate-leftappear hidden md:group-hover:block text-sm transition-all'>Se connecter</p>
                </button>
              </li>
            </ul>
          }
        
    </div>
  )
}

export default Menu