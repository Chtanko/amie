import { useState } from 'react'
import {BsPencilSquare} from 'react-icons/bs'
import { Lieu } from '../../../../typing'
import { Dialog } from '@headlessui/react'
import { LieuForm } from '../LieuForm'

export const ModifyLieuButton = ({lieu, modifyLieu, deleteLieu}: {lieu: Lieu, modifyLieu: any, deleteLieu: any}) => {
    const [isOpen, setIsOpen] = useState(false)
    const handleClick = () => {
      setIsOpen(true)
    }
    const state = (state:boolean) => {
      setIsOpen(state)
    }
  return (
    <div>
      <button onClick={handleClick} className='bg-secondary text-primary hover:bg-primary hover:text-base-100 p-2 rounded-lg transition-all'><BsPencilSquare className="w-6 h-6" /></button>
      <Dialog open={isOpen} onClose={setIsOpen} className="fixed inset-0 bg-white/60 backdrop-blur-sm overflow-auto">
        <Dialog.Overlay className="fixed inset-0 flex items-center justify-center p-4">
          <Dialog.Panel className="w-80 max-w-sm rounded-xl ring-1 ring-neutral/10 bg-white p-6 shadow-xl">
            <LieuForm lieu={lieu} modifyLieu={modifyLieu} deleteLieu={deleteLieu} state={state}/>
          </Dialog.Panel>
        </Dialog.Overlay>
      </Dialog>
    </div>
  )
}