import { Lieu } from '../../../typing'

export const LieuForm = ({lieu, modifyLieu, deleteLieu, state}: {lieu: Lieu, modifyLieu: any, deleteLieu: any, state: any}) => {
  const modifyThisLieu = async (e: any) => {
    e.preventDefault()
    const formData = new FormData(e.currentTarget);
    const entries = Object.fromEntries(formData.entries());
    const body = {
        "adresse": entries.adresse,
        "codePostal": entries.codePostal,
        "coordoneesGps": entries.coordoneesGps,
        "localisation": entries.localisation,
        "places": entries.places,
        "ville": entries.ville,
    }
    modifyLieu(lieu.idLieu, body)
    state(false)
  }

  const deleteThisLieu = async (e:any) => {
    e.preventDefault();
    deleteLieu(lieu);
    state(false)
  }

  return (
    <form className='grid gap-2' onSubmit={modifyThisLieu}>
        <input type="text" name='id' defaultValue={lieu.idLieu} className='hidden'/>
        <input className='capitalize p-4 input input-bordered' defaultValue={lieu.adresse} name='adresse' placeholder='Adresse'/>
        <input className=' capitalize p-4 input input-bordered' defaultValue={lieu.codePostal} name='codePostal' placeholder='Code Postal'/>
        <input className='text-sm p-4 input input-bordered'defaultValue={lieu.ville} name='ville' placeholder='Ville'/>
        <input name='localisation' type={'text'} defaultValue={lieu.localisation} className='p-4 input input-bordered' placeholder='Localisation'/>
        <input type="number" name='places' defaultValue={lieu.places} placeholder='Nombre de places' className='input p-4 input-bordered'/>
        <div className="grid grid-cols-2 gap-2">
          <button className='btn btn-primary text-base-100' type='submit'>Modifier</button>
          <button className='btn btn-error' onClick={deleteThisLieu}>Supprimer</button>
        </div>
    </form>
  )
}
