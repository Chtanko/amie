import React from 'react'

export const Board = ({className, title, children} : {className: string; title: string; children: React.ReactNode;}) => {
  return (
    <div className={className}>
        <h1 className={`col-span-full text-3xl font-semibold h-16 self-center`}>{title}</h1>
        {children}
    </div>
  )
}