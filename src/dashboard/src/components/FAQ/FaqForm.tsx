import { Faq } from '../../../typing';


const FaqForm = ({faq, modifyFaq, deleteFaq, state}: {faq: Faq, modifyFaq: any, deleteFaq: any, state: any}) => {
    const modifyThisFaq = async (e: any) => {
      e.preventDefault()
        const formData = new FormData(e.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        const body = {
          "question": entries.question,
          "description": entries.description,
        }
    
        modifyFaq(faq.idFaq, body)
        state(false)
      }
    
      return (
        <form className='grid gap-2' onSubmit={modifyThisFaq}>
            <input type="text" defaultValue={faq.idFaq} className='hidden'/>
            <input className='p-4 input input-bordered' defaultValue={faq.question} name='question'/>
            <input className='p-4 input input-bordered' defaultValue={faq.description} name='description' />
            <div className="grid grid-cols-2 gap-2">
              <button className='btn btn-primary text-base-100' type='submit'>Modifier</button>
              <button className='btn btn-error' onClick={(e) => {e.preventDefault(); deleteFaq(faq); state(false)}}>Supprimer</button>
            </div>
        </form>
      )
}

export default FaqForm