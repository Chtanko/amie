import { useQuery } from "@tanstack/react-query";
import { fetchUsers } from "../../api";
import { User } from "../../../typing";

export const AddFaqForm = ({addFaq, state}: {addFaq: any, state: any}) => {
    const users = useQuery({
        queryKey: ['users'],
        queryFn: fetchUsers
    })
    const addNewFaq = async (event: any) => {
        event.preventDefault()
        const formData = new FormData(event.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        const body = {
            "description": entries.description,
            "question": entries.question,
        }
        addFaq(body, Number(entries.user))
        state(false)

    }
    
  return (
    <form className='grid gap-2 h-full' onSubmit={addNewFaq}>
        <input className='input input-bordered p-4' name={'question'} placeholder='Question'/>
        <input className='input input-bordered p-4' name={'description'} placeholder='Réponse' />
        <select name="user" id="user" className="input input-bordered w-full outline-none">
            {   users.data?.map((user: User)  =>
                    <option value={user.idUtilisateur} key={user.idUtilisateur}>{user.prenom} {user.nom}</option>
                )
            }
        </select>
        <button className='bottom-3 btn btn-primary text-base-100' type='submit'>Ajouter</button>
    </form>
  )
}