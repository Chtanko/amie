export const AddUserForm = ({addUser, state}: {addUser:any, state:any}) => {

    const addNewUser = async (event: any) => {
        event.preventDefault()
        const formData = new FormData(event.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        const body = {
            "mail": entries.mail,
            "motDePasse": entries.motDePasse,
            "nom": entries.nom,
            "prenom": entries.prenom
        }
        addUser(body)
        state(false)

    }
    
  return (
    <form className='grid gap-2 h-full' onSubmit={addNewUser}>
        <input className='capitalize input input-bordered p-4' name={'prenom'} placeholder='Prénom'/>
        <input className=' uppercase input input-bordered p-4' name={'nom'} placeholder='Nom' />
        <input type="email" name="mail" className="input input-bordered p-4" placeholder="Email"/>
        <input type="password" name="motDePasse" className="input input-bordered p-4" placeholder="Mot de passe"/>
        <button className='bottom-3 btn btn-primary text-base-100' type='submit'>Ajouter</button>
    </form>
  )
}
