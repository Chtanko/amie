import { Dialog } from "@headlessui/react";
import { useState } from "react";
import { AiOutlinePlus } from "react-icons/ai";
import { RoleForm } from "../RoleForm";

export const AddRoleButton = ({id, addRole}: {id:number, addRole:any}) => {
    const [isOpen, setIsOpen] = useState(false)
    const handleClick = () => {
      setIsOpen(true)
    }

    const state = (state:boolean) => {
      setIsOpen(state)
    }
  return (
      <div>
        <button onClick={handleClick}><AiOutlinePlus className="p-1 bg-secondary text-primary rounded-lg invisible group-hover:visible hover:bg-primary hover:text-white active:scale-90" size={25} /></button>
        <Dialog open={isOpen} onClose={setIsOpen} className="fixed inset-0 bg-white/60 backdrop-blur-sm overflow-auto">
        <Dialog.Overlay className="fixed inset-0 flex items-center justify-center p-4">
          <Dialog.Panel className="w-80 max-w-sm rounded-xl ring-1 ring-neutral/10 bg-white p-6 shadow-xl">
            <RoleForm addRole={addRole} state={state} id={id}/>
          </Dialog.Panel>
        </Dialog.Overlay>
      </Dialog>
      </div>
      )
}
