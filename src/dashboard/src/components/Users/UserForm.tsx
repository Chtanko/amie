import { User } from '../../../typing'

export const UserForm = ({user, deleteUser, modifyUser, state}: {user: User, deleteUser: any, modifyUser: any, state: any}) => {
  
  const setModifyUser = async () => {
    const modifiedUser: User = {
      // @ts-ignore
      mail: document.getElementById('mail').value,
      motDePasse: user.motDePasse,
      // @ts-ignore
      nom: document.getElementById('nom').value,
      // @ts-ignore
      prenom: document.getElementById('prenom').value,
      dateDerniereInitialisationMotDePasse: '',
      idUtilisateur: user.idUtilisateur,
      roles: user.roles
    }

    modifyUser(modifiedUser)
  }

  return (
    <form className='grid gap-2'>
        <input type="text" defaultValue={user.idUtilisateur} className='hidden'/>
        <input className='capitalize p-4 input input-bordered' defaultValue={user.prenom} id='prenom'/>
        <input className=' uppercase p-4 input input-bordered' defaultValue={user.nom} id='nom' />
        <input className='text-sm p-4 input input-bordered'defaultValue={user.mail} id='mail' type={'email'}/>
        <div className="grid grid-cols-2 gap-2">
          <button className='btn btn-primary text-base-100' onClick={(e) => {e.preventDefault() ; setModifyUser(); state(false)}}>Modifier</button>
          <button className='btn btn-error' onClick={(e) => {e.preventDefault() ; deleteUser(user); state(false)}}>Supprimer</button>
        </div>
    </form>
  )
}
