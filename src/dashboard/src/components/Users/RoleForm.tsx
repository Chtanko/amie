import { useQuery } from '@tanstack/react-query';
import { Role} from '../../../typing'
import { fetchRoles } from '../../api';

export const RoleForm = ({id, state, addRole}:{id: number, state:any, addRole:any}) => {
  const roles = useQuery({
    queryKey:["roles"],
    queryFn: fetchRoles
  })

  const modifyRole = async (e:any) => {
    e.preventDefault()
    // @ts-ignore
    const idRole: string = document.getElementById('role')?.value
    addRole(String(id), idRole);
    state(false)
  }


  return (
    <form className='grid gap-2' onSubmit={modifyRole}>
        {
            roles.data ? 
        
            <select name="role" id="role" className='select select-primary'>
                {
                    roles.data.map((role: Role) => 
                        <option value={role.idRole} key={role.idRole}>{role.libelle}</option>    
                    )
                }
            </select>

            :

            <span className='loading loading-dots loading-lg text-primary'></span>
        }
        <div className="grid gap-2">
          <button className='btn btn-primary text-base-100' type='submit'>Ajouter</button>
        </div>
    </form>
  )
}
