import { useQuery } from '@tanstack/react-query'  
import { AiOutlineCalendar, AiOutlineClockCircle, AiOutlineEnvironment, AiOutlineTag } from 'react-icons/ai';
import { Evenment, Lieu, TypoEvent} from '../../../typing';
import {GiTeacher} from 'react-icons/gi'
import { VscInfo } from 'react-icons/vsc';
import { BsClipboardCheck } from 'react-icons/bs';
import { HiDocumentSearch } from 'react-icons/hi';
import { MdOndemandVideo, MdOutlineTitle } from 'react-icons/md';
import { fetchLieux, fetchTypo } from '../../api';
import { useState } from 'react';
import {RxCross2} from 'react-icons/rx'
import defaultImage from '/src/assets/visuel_absent.png'


export const EventForm = ({event, modifyEvent, state}: {event: Evenment, modifyEvent: any, state: any}) => {
    const [photo, setPhoto] = useState(event.image)
    const lieux = useQuery({
        queryKey: ['lieuData'],
        queryFn: fetchLieux
    })
    const typoEvent = useQuery({
        queryKey:['typos'],
        queryFn: fetchTypo
    })
    const [newTag, setNewTag] = useState(event.typologieEvenements)

    const getBase64 = (file: Blob) => {
        let reader = new FileReader();
        let res: any = ""
        reader.readAsDataURL(file);
        reader.onload = function () {
          res = reader.result?.toString().replace(/^data:(.*,)?/, '')
          setPhoto(res)
        };
        reader.onerror = function (error) {
          res = 'Error ', error
        };
    }

    const modifyThisEvent = async (e: any) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        const entries = Object.fromEntries(formData.entries());
        const newTypo = newTag.map((tag: TypoEvent) => +tag.idTypologieEvenements)
        const dateFormatter = (date: any) => {
            const formatedDate = new Date(date)
            const options = { year: "numeric", month: "numeric", day: "numeric" };
            // @ts-ignore
            return formatedDate.toLocaleDateString(undefined, options)
        }
        const body = {
            "date_debut": dateFormatter(entries.date_debut),
            "date_fin": dateFormatter(entries.date_fin),
            "description": entries.description,
            "heure_debut": entries.heure_debut,
            "heure_fin": entries.heure_fin,
            "label": entries.label,
            "idLieu" : +entries.lieux,
            "idTypologieEvenements": newTypo,
            "image": photo,
            "lien_replay": entries.replay,
            "lien_ressources": entries.ressources,
            "statut": entries.statut
        }
        modifyEvent(event.utilisateur.idUtilisateur, event.idEvenement, body)
        state(false)
    }
    
  return (
    <form className='asolute top-1/3 grid gap-2 p-2 rounded-xl m-auto' onSubmit={modifyThisEvent}>
            {/* l'image de l'event */}
            <label>
                <img src={photo ? `data:image;base64, ${photo}` : defaultImage} alt="Image de l'évènement" className='w-full h-20 flex items-center justify-center border rounded-lg object-cover'/>
                {/* @ts-ignore */}
                <input type="file" name="photo" id="photo" onChange={(e) => getBase64(e.currentTarget.files[0])} className="file-input file-input-bordered file-input-secondary file-input-sm mt-1 w-full"/>
            </label>
            {/* Le titre de l'event */}
            <label className='flex items-center'>
                <MdOutlineTitle size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input 
                    className='input input-bordered text-xl font-medium w-full outline-none first-letter:capitalize w' 
                    name='label'
                    placeholder="Titre"
                    defaultValue={event.label}
                />
            </label>
            {/* La c'est la typologie de l'évènement mais on verra ça plus tard */}
            <label className='flex items-center'>
                <AiOutlineTag size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="typo" id="typo" defaultValue={event.typologieEvenements[0].label} onChange={(e) => {newTag.push(JSON.parse(e.currentTarget.value)); setNewTag([...newTag]);}} className='input input-bordered w-full outline-none'>
                    {
                        typoEvent.data?.map((typo: TypoEvent) =>
                            <option key={typo.idTypologieEvenements} value={`{"idTypologieEvenements":"${typo.idTypologieEvenements}", "label":"${typo.label}"}`}>{typo.label}</option>
                        )
                    }
                </select>
            </label>
                <div className='w-full flex justify-around'>
                    {newTag.map((typo: TypoEvent, index: number) =>
                        <span key={typo.idTypologieEvenements} className='bg-secondary flex items-center text-primary px-2 rounded-full'>{typo.label} <button className='inline-block ml-2' onClick={() => {newTag.splice(index, 1); setNewTag([...newTag])}}><RxCross2 /></button> </span>
                    )}
                </div>
             <label className='flex items-center'>
                <VscInfo size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='description'
                    type="text-area"
                    defaultValue={event.description}
                />
            </label>
            <label className='flex items-center'>
                <AiOutlineCalendar size={"55"} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <div>
                    <input type="date" name="date_debut" defaultValue={event.date_debut} className="input input-bordered"/>
                    <input type="date" name="date_fin" defaultValue={event.date_fin} className="input input-bordered"/>
                </div>
                
            </label>
            <label className='flex items-center'>
                <AiOutlineClockCircle size={"55"} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <div>
                    <input type="time" name="heure_debut" defaultValue={event.heure_debut} className="input input-bordered"/>
                    <input type="time" name="heure_fin" defaultValue={event.heure_fin} className="input input-bordered"/>
                </div>
                
            </label>
            <label className='flex items-center'>
                <AiOutlineEnvironment size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="lieux" id="lieux" className="input input-bordered w-full outline-none" defaultValue={event.lieu?.idLieu}>
                    {lieux.data?.map((lieu: Lieu) => 
                           <option value={lieu.idLieu} key={lieu.idLieu} selected={lieu.idLieu === event.lieu?.idLieu}>{lieu.ville} : {lieu.localisation}</option>
                    )}
                </select>
            </label>
            <label className='flex items-center'>
                <GiTeacher size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='organizer'
                />
            </label>
            <label className='flex items-center'>
                <BsClipboardCheck size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="statut" id="statut" className="input input-bordered w-full outline-none" defaultValue={event.statut}>
                    <option value="En Attente">En attente</option>
                    <option value="Accepté">Accepté</option>
                    <option value="Refusé">Refusé</option>
                </select>
            </label>
            <label className='flex items-center'>
                <MdOndemandVideo size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='replay'
                    placeholder="Lien du replay"
                    defaultValue={event.lien_replay}
                />
            </label>
            <label className='flex items-center'>
                <HiDocumentSearch size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='ressources'
                    placeholder="Lien des ressources"
                    defaultValue={event.lien_ressources}
                />
            </label>
            <button className='btn btn-primary text-base-100' type='submit'>Modifier</button>
        </form>
  )
}
