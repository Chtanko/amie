import {
    useQuery,
} from '@tanstack/react-query'  
import { AiOutlineCalendar, AiOutlineClockCircle, AiOutlineEnvironment, AiOutlineTag, AiOutlineUser } from "react-icons/ai";
import { Lieu, TypoEvent, User } from "../../../typing";
import { GiTeacher } from "react-icons/gi";
import { BsClipboardCheck } from "react-icons/bs";
import { VscInfo } from "react-icons/vsc";
import { MdOndemandVideo, MdOutlineTitle } from "react-icons/md";
import { HiDocumentSearch } from "react-icons/hi";
import { fetchLieux, fetchTypo, fetchUsers } from '../../api';
import { useState } from 'react';
import { RxCross2 } from 'react-icons/rx';
import defaultImage from '/src/assets/visuel_absent.png'

export const AddEventForm = ({addEvent, state} : {addEvent:any, state:any}) => {
    const lieux = useQuery({
        queryKey: ['lieuData'],
        queryFn: fetchLieux
    })
        
    const users = useQuery({
        queryKey: ['users'],
        queryFn: fetchUsers
    })

    const typoEvent = useQuery({
        queryKey:['typos'],
        queryFn: fetchTypo
    })

    const [newTag, setNewTag] : any[] = useState([])

    const [photo, setPhoto] = useState("")
    const getBase64 = (file: Blob) => {
        let reader = new FileReader();
        let res: any = ""
        reader.readAsDataURL(file);
        reader.onload = function () {
          res = reader.result?.toString().replace(/^data:(.*,)?/, '')
          setPhoto(res)
        };
        reader.onerror = function (error) {
          // console.log('Error ', error);
          res = 'Error ', error
        };
        
        
    }
    const onChange = (event: any) => {
        const typo = JSON.parse(event.currentTarget.value)
        newTag.filter((tag: TypoEvent) => tag.idTypologieEvenements === typo.idTypologieEvenements).length === 0 ? newTag.push(typo) : setNewTag([...newTag]) ;
        setNewTag([...newTag]);
    }
    const addNewEvent = async (event: any) => {
        event.preventDefault()

        const formData = new FormData(event.currentTarget);
        const entries = Object.fromEntries(formData.entries()); 
        const newTypo = newTag.map((tag: TypoEvent) => tag.idTypologieEvenements)
        const dateFormatter = (date: any) => {
            const formatedDate = new Date(date)
            const options = { year: "numeric", month: "numeric", day: "numeric" };
            // @ts-ignore
            return formatedDate.toLocaleDateString(undefined, options)
        }

        const body = {
            "date_debut": dateFormatter(entries.date_debut),
            "date_fin": dateFormatter(entries.date_fin),
            "description": entries.description,
            "heure_debut": entries.heure_debut,
            "heure_fin": entries.heure_fin,
            "idTypologieEvenements": newTypo,
            "image": photo,
            "label": entries.label,
            "lien_replay": entries.replay,
            "lien_ressources": entries.ressources,
            "statut": entries.statut
        }

        addEvent(String(entries.creator), String(entries.lieux), body)
        state(false)
    }
    
  return (
    <form className='grid gap-2 p-2 rounded-xl m-auto' onSubmit={addNewEvent}>
            {/* l'image de l'event */}
            <img src={photo === "" ? defaultImage : (`data:image;base64, ${photo}`)} className='w-full h-20 object-cover rounded-lg'/>
            {/* @ts-ignore */}
            <input type="file" name="photo" id="photo" onChange={(e) => getBase64(e.currentTarget.files[0])} className="file-input file-input-bordered file-input-secondary file-input-sm mt-1 w-full"/>
            {/* Le titre de l'event */}
            <label className='flex items-center'>
                <MdOutlineTitle size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input 
                    className='input input-bordered text-xl font-medium w-full outline-none first-letter:capitalize' 
                    name='label'
                    placeholder="Titre"
                />
            </label>
            <label className='flex items-center'>
                <AiOutlineTag size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="typo" id="typo"onChange={(e) => onChange(e)} className='input input-bordered w-full outline-none' defaultValue={''}>
                    <option> Typologie </option>
                    {
                        typoEvent.data?.map((typo: TypoEvent) =>
                            <option key={typo.idTypologieEvenements} value={`{"idTypologieEvenements":"${typo.idTypologieEvenements}", "label":"${typo.label}"}`}>{typo.label}</option>
                        )
                    }
                </select>
            </label>
            <div className='w-full flex justify-around'>
                    {newTag.map((typo: TypoEvent, index: number) =>
                        <span key={typo.idTypologieEvenements} className='bg-secondary flex items-center text-primary px-2 rounded-full'>{typo.label} <button className='inline-block ml-2' onClick={() => {newTag.splice(index, 1); setNewTag([...newTag])}}><RxCross2 className=' border border-primary rounded-full hover:bg-primary hover:text-white'/></button> </span>
                    )}
                </div>
             <label className='flex items-center'>
                <VscInfo size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='description'
                    type="text"
                    minLength={2}
                    required
                    placeholder="Description"
                />
            </label>
            <label className='flex items-center'>
                <AiOutlineCalendar size={"55"} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <div className=''>
                    <input type="date" name="date_debut" placeholder="Date de début : JJ/MM/AAAA" className="w-40 mr-2 input input-bordered"/>
                    <input type="date" name="date_fin" placeholder="Date de fin : JJ/MM/AAAA" className="w-40 input input-bordered"/>
                </div> 
            </label>
            <label className='flex items-center'>
                <AiOutlineClockCircle size={"55"} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <div className=''>
                    <input required type="time" name="heure_debut" placeholder="Heure de début : hh:mm" className="w-40 mr-2 input input-bordered"/>
                    <input required type="time" name="heure_fin" placeholder="heure de fin : hh:mm" className="w-40 input input-bordered"/>
                </div>
            </label>
            <label className='flex items-center'>
                <AiOutlineEnvironment size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2' />
                <select name="lieux" id="lieux" className="input input-bordered w-full outline-none">
                    { lieux.data?.map((lieu: Lieu) => (
                        <option value={lieu.idLieu} key={lieu.idLieu} className="text-sm" >{lieu.adresse}, {lieu.ville} : {lieu.localisation}</option>
                    ))}
                </select>
            </label>
            <label className='flex items-center'>
                <AiOutlineUser size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="creator" id="creator" className="input input-bordered w-full outline-none">
                    { 
                        users.data?.map((user: User)  =>
                            <option value={user.idUtilisateur} key={user.idUtilisateur}>{user.prenom} {user.nom}</option>
                        )
                    }
                </select>
            </label>
            <label className='flex items-center'>
                <GiTeacher size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='organizer'
                />
            </label>
            <label className='flex items-center'>
                <BsClipboardCheck size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <select name="statut" id="statut" className="input input-bordered w-full outline-none">
                    <option value="">Statut</option>
                    <option value="En Attente">En attente</option>
                    <option value="Accepté">Accepté</option>
                    <option value="Refusé">Refusé</option>
                </select>
            </label>
            <label className='flex items-center'>
                <MdOndemandVideo size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='replay'
                    placeholder="Lien du replay"
                />
            </label>
            <label className='flex items-center'>
                <HiDocumentSearch size={'55'} className='bg-secondary p-4 text-primary rounded-xl mr-2'/>
                <input
                    className='input input-bordered w-full'
                    name='ressources'
                    placeholder="Lien des ressources"
                />
            </label>
            <button className='btn btn-primary text-white' type='submit'>Ajouter</button>
        </form>
  )
}
