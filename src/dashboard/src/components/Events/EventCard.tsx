import { AiOutlineCalendar, AiOutlineInfoCircle } from 'react-icons/ai';
import { HiDocumentSearch, HiOutlineUsers } from 'react-icons/hi';
import { FaMapMarkerAlt } from 'react-icons/fa';
import { BsCameraVideo } from 'react-icons/bs';
import { MdOndemandVideo, } from 'react-icons/md'
import { Evenment, Inscription, TypoEvent } from '../../../typing';
import { ModifyEventButton } from './Buttons/ModifyEventButton';
import { Link } from 'react-router-dom';
import { DeleteEventButton } from './Buttons/DeleteEventButton';
import { useQuery } from '@tanstack/react-query';
import { fetchInscriptionEvent } from '../../api';
import defaultImage from '/src/assets/visuel_absent.png'

function EventCard({event, deleteEvent, modifyEvent} : {event: Evenment, deleteEvent: any, modifyEvent: any}) {
  const inscrits = useQuery({
    queryKey: ['inscrits'],
    queryFn: fetchInscriptionEvent
  })

  return (
      <div className="group relative bg-base-100 h-full border rounded-lg p-2 border-secondaryl shadow-mgen hover:shadow-[0_0_10px_0_rgba(106,165,23,0.32)] transition-all">
        <div className='absolute inset-0 group-hover:backdrop-blur rounded-xl transition-all'></div>
        <ModifyEventButton event={event} modifyEvent={modifyEvent}/>
        <DeleteEventButton event={event} deleteEvent={deleteEvent}/>
        <div className="grid gap-4 h-full">
          { event.image ?
            <img src={`data:image;base64, ${event.image}`} className='w-full h-20 object-cover rounded-lg'/>
            :
            <img src={defaultImage} alt="image par défaut" className='w-full h-20 object-cover rounded-lg' />
          }
          <h3 className='text-xl font-medium ml-2 first-letter:capitalize'>{event.label}</h3>
          <div className='grid-cols-[4rem_1fr] grid gap-4'>
            <div className='flex col-span-2 gap-2 flex-wrap'>
              {
                event.typologieEvenements.map((tag: TypoEvent) =>
                  <span key={tag.idTypologieEvenements} className='badge badge-secondary col-span-2 text-primary'>{tag.label}</span>
                )
              }
            </div>
            <AiOutlineInfoCircle size={35} className=' text-primary mx-4 bg-primary/10 rounded-lg p-2'/>
            <span className='font-thin text-sm flex items-center'>{event.description}</span>
            <AiOutlineCalendar size={35} className=' text-primary mx-4 bg-primary/10 rounded-lg p-2'/>
            <span className=' flex font-light text-sm items-center'> {event.date_debut} - {event.date_fin}</span>
            {(event.lieu === null ) ? <BsCameraVideo size={35} className='text-primary mx-4 bg-primary/10 rounded-lg p-2'/> : <FaMapMarkerAlt size={35} className='text-primary mx-4 bg-primary/10 rounded-lg p-2'/> }
            <span className='flex font-thin items-center'>
              {event.lieu ? event.lieu.ville  + " : " + event.lieu.localisation : "En ligne"}
            </span>
            <HiOutlineUsers size={35} className=' text-primary mx-4 bg-primary/10 rounded-lg p-2'/>
            <span>
              {
                inscrits.isFetching ?
                  "..."
                  :
                  
                  inscrits.data.filter((inscription: Inscription) => inscription.idEvenement === event.idEvenement).length
                
              }
              /
              {event.lieu?.places}
            </span>
          </div>
          
          <div className={`${(!event.lien_replay && !event.lien_ressources) ? 'hidden':'block'} eventCard-replay`} >
            <h4 className='col-span-2 font-semibold'>Liens ressources :</h4>
            <div className='flex justify-center my-2'> 
              <Link to={event.lien_replay} className='flex place-content-center'>
                <MdOndemandVideo size={35} className='text-base-100 mx-4 bg-primary rounded-lg p-2 border border-primary'/>
              </Link>
              <Link to={event.lien_ressources} className='flex place-content-center'>
                <HiDocumentSearch size={35} className='text-base-100 mx-4 bg-primary rounded-lg p-2 border border-primary'/>
              </Link>
            </div>
          </div>
        </div>
      </div>
  )
}

export default EventCard