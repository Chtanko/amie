import { Evenment } from '../../../typing'

export const EventDeleteAlert = ({event, deleteEvent, state} : {event: Evenment, deleteEvent: any, state: any}) => {
  
  const deleteThisEvent = async (e: any) => {
    e.preventDefault()
    const atId = Object.fromEntries(new FormData(e.currentTarget).entries()).event
    await deleteEvent(+atId)
    state(false)
  }

  return (
    <form className='w-64 grid' onSubmit={deleteThisEvent}>
      <input type="text" value={event.idEvenement} readOnly name='event' className='hidden'/>
      <label>
        <h3 className='p-4 mb-2 text-lg font-medium text-center'>Etes-vous sûr de vouloir supprimer cet évènement ?</h3>
      </label>
      <button className='btn btn-error ' type='submit'>Supprimer</button>
    </form>
  )
}