# Amie Dashboard v2
Ce projet est la version 2 du dashboard de l'application AMIE. Il est build via le bundle Vite.js et utilise toute les fonctionnalités qu'offre le client-side rendering.

## Installation
Pour installer ce projet en local et build l'application, il vous faut entrer ces commandes :
```bash
git clone git@gitlab.krj.gie:labs/experimentations/amie-dashboard-2.git
pnpm install
pnpm dev || pnpm build
```